#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "iic.h"  
#include "servo.h"
#include "laser.h"
#include "imu.h"
#include "rawdata.h"
#include "delay.h"
#include "map.h"
#include "serial.h"
#include "display.h"
#include "testing.h"
#include <math.h>
#include "l3g4200.h"  // register's definitions    ( not used by ed )

/*!
  * \file testing.c
  * File that contains fuctions for basic device testing
*/

/// Move the servo through its minimum to maximum azimuth and display orientation
void servoAzimuthTest(void){
  unsigned azimuth = AZIMUTH_START;
  unsigned char azimuth_string[7];
  
  setElevation(0);
  LCD_clear();
  printLCD(1,0,"Azimuth:");
  delay1(500);
  
  while(azimuth <= AZIMUTH_END){
    float_to_str(azimuth,azimuth_string);
    printLCD(1,10,azimuth_string);
    setAzimuth(azimuth);
    azimuth += AZIMUTH_STEP; 
    delay1(500);  
  }
}

/// Move the servo through its minimum to maximum elevation and display orientation
void servoElevationTest(void){
  int elevation = ELEVATION_START;
  unsigned char elevation_string[7];
  
  setAzimuth(90);
  LCD_clear();
  printLCD(1,0,"Elevation:");
  delay1(500);
  
  while(elevation <= ELEVATION_END){
    float_to_str(elevation,elevation_string);
    printLCD(1,10,elevation_string);
    setElevation(elevation);
    elevation += ELEVATION_STEP;
    delay1(500);
  }
}

/// Operate the gyroscope and display orientation
void gyroTest(void){
    float azimuth;
    float elevation;
    unsigned char azimuth_string[7];
    unsigned char elevation_string[7];
    
    prepareGyro(GYRO_START);
    LCD_clear();
    printLCD(1,0,"Azimuth:");
    printLCD(2,0,"Elevation:");
    
    while(1){
      azimuth = returnGyroAzimuth();
      float_to_str(azimuth,azimuth_string);
      printLCD(1,10,azimuth_string);
      elevation = returnGyroElevation();
      float_to_str(elevation,elevation_string);
      printLCD(2,10,elevation_string);
      delay1(TEST_PERIOD_MS);  
    }
}

/// Operate the magnetometer and display orientation data
void magTest(void){
    float azimuth;
    unsigned char azimuth_string[7];
    
    prepareMag();
    LCD_clear();
    printLCD(1,0,"Azimuth:");
    
    while(1){
      azimuth = returnMagAzimuth();
      float_to_str(azimuth,azimuth_string);
      printLCD(1,10,azimuth_string);
      delay1(TEST_PERIOD_MS);  
    }  
}

/// Operate the accelerometer and display orientation data
void accTest(void){
    float elevation;
    unsigned char elevation_string[7];
    
    setElevation(0);
    setAzimuth(90);
    LCD_clear();
    printLCD(1,0,"Elevation:");
    
    while(1){
      elevation = returnAccElevation();
      float_to_str(elevation,elevation_string);
      printLCD(1,10,elevation_string);
      delay1(TEST_PERIOD_MS);  
    } 
}

/// Operate the laser and display range data
void laserTest(void){
    float range;
    unsigned char range_string[7];
    
    setElevation(0);
    setAzimuth(90);
    
    LCD_clear();
    printLCD(1,0,"Range:");
    
    while(1){
      range = laserRange();
      float_to_str(range,range_string);
      printLCD(1,10,range_string);
      delay1(TEST_PERIOD_MS);
    }
}