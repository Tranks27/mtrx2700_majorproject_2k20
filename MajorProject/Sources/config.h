//config definititions
#ifndef CONFIG_H
#define CONFIG_H

/// definition of config struct
typedef struct s_config{
  unsigned char headStart;    //!< Start azimuth value in degrees [20,160] 
  unsigned char headEnd;      //!< End azimuth value in degrees [20,160]
  unsigned char headStep;     //!< Azimuth step value in 10*degrees [1,100]
  signed char elevStart;      //!< Start elevation value in degrees [-70,70]
  signed char elevEnd;        //!< End elevation value in degrees [-70,70]
  unsigned char elevStep;     //!< Elevation step value in 10*degrees [1,100]
  unsigned int frequency;     //!< Sampling frequency in Hz [1,50]
  unsigned char samples;      //!< Number of samples per orientation [1,250]
  unsigned char control;      //!< see <control> below
  unsigned char elevCalc;     //!< chooses data for elevation calculation
  unsigned char headCalc;     //!> chooses data for azimuth calculation
}config;

/*!
<control>
bits 0 and 1 are azimuth display selection
  - 00, Servo
  - 01, Gyroscope
  - 10, Magnetometer
bits 2 and 3 are elevation display selection
  - 00, Servo
  - 01, Gyroscope
  - 10, Accelerometer
bit 7 is plotting selectioin
  - 0,  Offline
  - 1,  Online
*/

// functions

void initConfigVals(config *c);
void updateConfigs(config*,config*,config*);

// definitions

#define AZIMUTH_MAX 160      //!< Maximum azimuth
#define AZIMUTH_MIN 20       //!< Minimum azimuth
#define ELEVATION_MAX 70     //!< Maximum elevation
#define ELEVATION_MIN -70    //!< Minimum elevation
#define STEP_MAX 100         //!< Maximum step size
#define STEP_MIN 1           //!< Minimum step size
#define NO_SAMPLES_MAX 250   //!< Maximum samples per orientation
#define NO_SAMPLES_MIN 1     //!< Minimum samples per orientation
#define SAMPLE_FREQ_MAX 50   //!< Maximum sample frequency
#define SAMPLE_FREQ_MIN 1    //!< Minimum sample frequency
#define DEVICE_MAX 2         //!< Maximum device code
#define DEVICE_MIN 0         //!< Minimum device code
#define ELEVATION_OFFSET 70
#define SERVO_CODE 0         //!< Device code for servo
#define GYRO_CODE 1          //!< Device code for gyroscope
#define MAG_CODE 2           //!< Device code for magnetometer
#define ACC_CODE 2           //!< Device code for accelerometer
#define LCD_HEADING_MASK 0b00000011       //!< Control mask for Azimuth Display data
#define LCD_ELEVATION_MASK 0b00001100     //!< Control mask for Elevation Display data
#define PLOTTING_MASK 0b10000000          //!< Control mask for Plotting data


#endif
