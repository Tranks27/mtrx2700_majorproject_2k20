#include "menu_screen.h"
#include "config.h"
#include "keypad.h"
#include "display.h"
#include "delay.h"
#include "config.h"
#include <math.h>

/*!
  * \file menu_screen.c
  * File that contains functions for LCD menu sequences
*/

/// Main menu items
static const unsigned char config_menu1[13][16]={
  "Azimuth Start",
  "Azimuth End",
  "Elevation Start",
  "Elevation End",
  "Azimuth Step",
  "Elevation Step",
  "Samples",
  "Frequency",
  "Azimuth Disp",
  "Elevation Disp",
  "Plotting",
  "Azimuth Calc",
  "Elevation Calc" 
};

/// Possible values for LCD azimuth menu
static const unsigned char config_LCD_azimuth_menu[3][16]={
  "SERVO       ",
  "GYROSCOPE   ",
  "MAGNETOMETER"
};

/// Possible values for LCD elevation menu
static const unsigned char config_LCD_elevation_menu[3][16]={
  "SERVO        ",
  "GYROSCOPE    ",
  "ACCELEROMETER"
};

/// Possible values for plotting menu
static const unsigned char config_plotting_menu[2][16]={
  "OFFLINE",
  "ONLINE "
};

/// Menu items for display at run time
static const unsigned char disp_menu[2][16]={
  "Azimuth:",
  "Elevation:"
};

/// Other general menu items
static const unsigned char general_menu[1][16]={
  "Error!"
};

extern char programState;                  //!< state of program: 'C' for configuration or 'S' for scanning
extern config secondaryConfigs;            //!< storage of the configuration settings according to the keypad

bool START;                                //!< state of START key (pressed or not)
static unsigned int menuSelection;         //!< stores index of the the Main Menu
static bool RIGHT;                         //!< state of RIGHT key (pressed or not)
static bool LEFT;                          //!< state of LEFT key (pressed or not)
static bool EDIT_UP;                       //!< state of EDIT_UP key (pressed or not)
static bool EDIT_DOWN;                     //!< state of EDIT_DOWN key (pressed or not)
static bool REFRESH;

static unsigned char config_azimuth_start_str[5];
static unsigned char config_azimuth_end_str[5];
static unsigned char config_elevation_start_str[5];
static unsigned char config_elevation_end_str[5];
static unsigned char config_azimuth_step_str[5];
static unsigned char config_elevation_step_str[5];
static unsigned char config_no_samples_str[5];
static unsigned char config_sample_frequency_str[5];

/// Initialise variables for the menu startup
void initMenu(void){
    menuSelection = 0;        // Start from first item on Main Menu on startup
    RIGHT = true;             // set true to automatically show starting display
    LEFT = false;
    EDIT_UP = false;
    EDIT_DOWN = false;
    START = false;
}

// Refresh the current LCD display item
void refreshMenu(void){
  REFRESH = true;
}


/** \brief Start running the menu.
* Start from Main Menu
* Go through each menu item by using keypad
* Customize the values
*/
void startup_menu(void) {
   //Run the loop to check for to go through the menus.
        switch (menuSelection){            
            case 0:
                m_headingStart();
                break;
            case 1:     
                m_headingEnd();           
                break;
            case 2:     
                m_elevationStart();
                break;
            case 3:     
                m_elevationEnd();
                break;
            case 4:     
                m_headingStep();
                break;
            case 5:
                m_elevationStep();
                break;
            case 6:     
                m_samples();
                break;
            case 7:     
                m_frequency();
                break;
            case 8:     
                m_lcdHeading();
                break;
            case 9:     
                m_lcdElev();
                break;
            case 10:     
                m_plotting();
                break;
            case 11:
                m_calcHeading();
                break;
            case 12:
                m_calcElevation();
                break;
            default:
                printLCD(1, 0, general_menu[0]);
        }
        delay1(50);
        keycheck();   //check which key is pressed
        menu_shift();
    
}

/// headingStart configuration process
void m_headingStart(void){

    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[0]);
                       
        // print out the current value of headStart
        int_to_str(secondaryConfigs.headStart,config_azimuth_start_str);
        printLCD(2,0,config_azimuth_start_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false;     
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.headStart++;
          if(secondaryConfigs.headStart>AZIMUTH_MAX){
            secondaryConfigs.headStart = AZIMUTH_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.headStart--;
          if(secondaryConfigs.headStart<AZIMUTH_MIN){
            secondaryConfigs.headStart = AZIMUTH_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        int_to_str(secondaryConfigs.headStart,config_azimuth_start_str);
        printLCD(2,0,config_azimuth_start_str);        
    } 
}

/// headingEnd configuration process
void m_headingEnd(void){

    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[1]);
                       
        // print out the current value of headEnd
        int_to_str(secondaryConfigs.headEnd,config_azimuth_end_str);
        printLCD(2,0,config_azimuth_end_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.headEnd++;
          if(secondaryConfigs.headEnd>AZIMUTH_MAX){
            secondaryConfigs.headEnd = AZIMUTH_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.headEnd--;
          if(secondaryConfigs.headEnd<AZIMUTH_MIN){
            secondaryConfigs.headEnd = AZIMUTH_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        int_to_str(secondaryConfigs.headEnd,config_azimuth_end_str);
        printLCD(2,0,config_azimuth_end_str);        
    }   
}

/// ElevationStart configuration process
void m_elevationStart(void){

    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[2]);
                       
        // print out the current value of elevStart
        int_to_str(secondaryConfigs.elevStart,config_elevation_start_str);
        printLCD(2,0,config_elevation_start_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.elevStart++;
          if(secondaryConfigs.elevStart>ELEVATION_MAX){
            secondaryConfigs.elevStart = ELEVATION_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.elevStart--;
          if(secondaryConfigs.elevStart<ELEVATION_MIN){
            secondaryConfigs.elevStart = ELEVATION_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        int_to_str(secondaryConfigs.elevStart,config_elevation_start_str);
        printLCD(2,0,config_elevation_start_str);        
    }   
}

/// Elevation End configuration process
void m_elevationEnd(void){

    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[3]);
                       
        // print out the current value of elevEnd
        int_to_str(secondaryConfigs.elevEnd,config_elevation_end_str);
        printLCD(2,0,config_elevation_end_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.elevEnd++;
          if(secondaryConfigs.elevEnd>ELEVATION_MAX){
            secondaryConfigs.elevEnd = ELEVATION_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.elevEnd--;
          if(secondaryConfigs.elevEnd<ELEVATION_MIN){
            secondaryConfigs.elevEnd = ELEVATION_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        int_to_str(secondaryConfigs.elevEnd,config_elevation_end_str);
        printLCD(2,0,config_elevation_end_str);        
    } 
}

/// Heading Step configuration process
void m_headingStep(void){

    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[4]);
                       
        // print out the current value of headingStep
        float_to_str(((float)secondaryConfigs.headStep)/10,config_azimuth_step_str);
        printLCD(2,0,config_azimuth_step_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.headStep++;
          if(secondaryConfigs.headStep>STEP_MAX){
            secondaryConfigs.headStep = STEP_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.headStep--;
          if(secondaryConfigs.headStep<STEP_MIN){
            secondaryConfigs.headStep = STEP_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        float_to_str(((float)secondaryConfigs.headStep)/10,config_azimuth_step_str);
        printLCD(2,0,config_azimuth_step_str);        
    }       
}

/// Elevation Step configuration process
void m_elevationStep(void){
    
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[5]);
                       
        // print out the current value of elevStep
        float_to_str(((float)secondaryConfigs.elevStep)/10,config_elevation_step_str);
        printLCD(2,0,config_elevation_step_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.elevStep++;
          if(secondaryConfigs.elevStep>STEP_MAX){
            secondaryConfigs.elevStep = STEP_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.elevStep--;
          if(secondaryConfigs.elevStep<STEP_MIN){
            secondaryConfigs.elevStep = STEP_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        float_to_str(((float)secondaryConfigs.elevStep)/10,config_elevation_step_str);
        printLCD(2,0,config_elevation_step_str);        
    }  
}

/// Samples configuration process
void m_samples(void){
    
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[6]);
                       
        // print out the current value of noSamples
        int_to_str(secondaryConfigs.samples,config_no_samples_str);
        printLCD(2,0,config_no_samples_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.samples++;
          if(secondaryConfigs.samples>NO_SAMPLES_MAX){
            secondaryConfigs.samples = NO_SAMPLES_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.samples--;
          if(secondaryConfigs.samples<NO_SAMPLES_MIN){
            secondaryConfigs.samples = NO_SAMPLES_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        int_to_str(secondaryConfigs.samples,config_no_samples_str);
        printLCD(2,0,config_no_samples_str);        
    }
}

/// Frequency configuration process
void m_frequency(void){
     
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[7]);
                       
        // print out the current value of sampleFreq
        int_to_str(secondaryConfigs.frequency,config_sample_frequency_str);
        printLCD(2,0,config_sample_frequency_str);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.frequency++;
          if(secondaryConfigs.frequency>SAMPLE_FREQ_MAX){
            secondaryConfigs.frequency = SAMPLE_FREQ_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.frequency--;
          if(secondaryConfigs.frequency<SAMPLE_FREQ_MIN){
            secondaryConfigs.frequency = SAMPLE_FREQ_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        int_to_str(secondaryConfigs.frequency,config_sample_frequency_str);
        printLCD(2,0,config_sample_frequency_str);        
    } 
}

/// LCD heading display source configuration process
void m_lcdHeading(void){
    unsigned char value;
    value = secondaryConfigs.control & LCD_HEADING_MASK;
     
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[8]);
                       
        // print out the current value of LCD Heading
        printLCD(2,0,config_LCD_azimuth_menu[value]);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          value++;
          if(value>DEVICE_MAX){
            value = DEVICE_MIN;
          }
          EDIT_UP = false;
        }
        else{
          value--;
          if(value>DEVICE_MAX){
            value = DEVICE_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        secondaryConfigs.control = (secondaryConfigs.control &~(LCD_HEADING_MASK))|value; // update control byte
        printLCD(2,0,config_LCD_azimuth_menu[value]);        
    }   
}

/// LCD Elevation display source configuration process
void m_lcdElev(void){
    unsigned char value;
    value = (secondaryConfigs.control & LCD_ELEVATION_MASK)>>2;
     
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[9]);
                       
        // print out the current value of LCD Elevation
        printLCD(2,0,config_LCD_elevation_menu[value]);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          value++;
          if(value>DEVICE_MAX){
            value = DEVICE_MIN;
          }
          EDIT_UP = false;
        }
        else{
          value--;
          if(value>DEVICE_MAX){
            value = DEVICE_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        secondaryConfigs.control = (secondaryConfigs.control &~(LCD_ELEVATION_MASK))|(value<<2);  // update control byte
        printLCD(2,0,config_LCD_elevation_menu[value]);        
    }   
}

/// Plotting configuration process
void m_plotting(void){
    unsigned char value;
    value = (secondaryConfigs.control & PLOTTING_MASK)>>7;
     
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[10]);
                       
        // print out the current value of LCD Elevation
        printLCD(2,0,config_plotting_menu[value]);
        LEFT = false;
        RIGHT = false; 
        REFRESH = false;
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
          value = !value;
          EDIT_UP = false;
          EDIT_DOWN = false;
        
        // display updated value
        secondaryConfigs.control = (secondaryConfigs.control &~(PLOTTING_MASK))|(value<<7); // update control byte
        printLCD(2,0,config_plotting_menu[value]);        
    }   
}

/// Heading calculation device configuration process
void m_calcHeading(void){
  
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[11]);
                       
        // print out the current value
        printLCD(2,0,config_LCD_azimuth_menu[secondaryConfigs.headCalc]);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.headCalc++;
          if(secondaryConfigs.headCalc>DEVICE_MAX){
            secondaryConfigs.headCalc = DEVICE_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.headCalc--;
          if(secondaryConfigs.headCalc>DEVICE_MAX){
            secondaryConfigs.headCalc = DEVICE_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        printLCD(2,0,config_LCD_azimuth_menu[secondaryConfigs.headCalc]);        
    }   
}

/// Elevation calculation device configuration process
void m_calcElevation(void){
  
    if(LEFT||RIGHT||REFRESH){
        LCD_clear();
        printLCD(1,0, config_menu1[12]);
                       
        // print out the current value
        printLCD(2,0,config_LCD_elevation_menu[secondaryConfigs.elevCalc]);
        LEFT = false;
        RIGHT = false;
        REFRESH = false; 
    }

    if(EDIT_UP||EDIT_DOWN){
        
        // update stored value
        if(EDIT_UP){
          secondaryConfigs.elevCalc++;
          if(secondaryConfigs.elevCalc>DEVICE_MAX){
            secondaryConfigs.elevCalc = DEVICE_MIN;
          }
          EDIT_UP = false;
        }
        else{
          secondaryConfigs.elevCalc--;
          if(secondaryConfigs.elevCalc>DEVICE_MAX){
            secondaryConfigs.elevCalc = DEVICE_MAX;
          }
          EDIT_DOWN = false;
        }
        
        // display updated value
        printLCD(2,0,config_LCD_elevation_menu[secondaryConfigs.elevCalc]);        
    }   
}

/** \brief Function to covert an integer to a string. 
* Divides up the integer value to single digits and covert them to char type. 
* Then put them in an array with NULL at the end to create a string 
*/
void int_to_str(int value,unsigned char* string){

  int magnitude_int;
  char sign;  
  int first_digit,first_remainder,second_digit,third_digit;
  
  magnitude_int = fabs(value);
  
  // determine sign of int
  if(value<0){
    sign = '-';
  } 
  else{
    sign = '+';
  }
  
  // determine each digit
  first_digit = magnitude_int/100;
  first_remainder = magnitude_int%100;
  second_digit = first_remainder/10;
  third_digit = first_remainder%10;
  
  // convert to chars and update string
  string[0] = sign;
  string[1] = first_digit + '0';
  string[2] = second_digit + '0';
  string[3] = third_digit + '0';
  string[4] = '\0';

}

/// Check which key is pressed
void keycheck(void){
    unsigned char currentKey = find_keyPressed();
    if(currentKey == '3'){
        RIGHT = true;    
    }else if(currentKey == '2'){
        LEFT = true;
    }else if(currentKey == '6'){
        EDIT_UP = true;
    }else if(currentKey == '5'){
        EDIT_DOWN = true;
    }else if(currentKey == '9'){
        START = true;
    }
}

/// Shift menu items left or right depending on the key pressed
void menu_shift(void){
    if(RIGHT){
        // right of last menu is first menu   
        if(menuSelection == 12){
          menuSelection = 0;
        }else{
          menuSelection++;
        }    
    }
    if(LEFT){
        // left of first menu is last menu 
        if(menuSelection == 0){
          menuSelection = 12;
        }else{
          menuSelection--;
        }
    }   
}

