#ifndef TESTING_H
#define TESTING_H

void servoAzimuthTest(void);
void servoElevationTest(void);
void gyroTest(void);
void magTest(void);
void accTest(void);
void laserTest(void);

#define AZIMUTH_START 20           
#define AZIMUTH_END 160
#define AZIMUTH_STEP 5

#define ELEVATION_START -70
#define ELEVATION_END 70
#define ELEVATION_STEP 5

#define GYRO_START 90
#define TEST_PERIOD_MS 100

#endif