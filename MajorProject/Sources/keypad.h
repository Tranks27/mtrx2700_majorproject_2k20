#ifndef KEYPAD_H
#define KEYPAD_H

 void init_keypad(void);
 unsigned char find_keyPressed(void);
 unsigned char keyScanner(unsigned int);
 
#endif