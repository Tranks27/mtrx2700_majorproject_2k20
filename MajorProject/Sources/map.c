#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "iic.h"  
#include "servo.h"
#include "laser.h"
#include "imu.h"
#include "rawdata.h"
#include "delay.h"
#include "map.h"
#include "serial.h"
#include "display.h"
#include <math.h>
#include "l3g4200.h"  // register's definitions    ( not used by ed )

/*!
  * \file map.c
  * File that contains functions to map the room by moving servos around and recording their position and the measured range
*/

static float range_min;               //!< Range to closest object
static float range_min_azimuth;       //!< Azimuth of closest object
static float range_min_elevation;     //!< Elevation of closest object

// current laser range, this must be accessed by the 7 seg interrupt
float map_laser_range;      //!< Value for laser range

/// Function that moves servos around and takes measurements from laser and stores/outputs them to the display module.
void mapRoom(config c){

  // storage of data used for mapping
  float map_gyro_elevation;       //!< Value returned by gyroscope for elevation
  float map_gyro_azimuth;         //!< Value returned by gyroscope for azimuth
  float map_mag_azimuth;          //!< Value returned by magnetometer for azimuth
  float map_acc_elevation;        //!< Value returned by accelerometer for elevation
  float x_data;                   //!< x value of laser target
  float y_data;                   //!< y value of laser target
  float z_data;                   //!< z value of laser target
  float sum;                      // Variable used to calculate average of samples

  unsigned char azimuth_string[7];    //!< String that contains azimuth display data
  unsigned char elevation_string[7];  //!< String that contains elevation display data
 
  float azimuth;     //!< Value for azimuth from servo
  float elevation;        //!< Value for elevation from servo
  int i;
  float calc_azimuth;     //!< Device used for azimuth calculations
  float calc_elevation;   //!< Device used for elevation calculations
  
  char azimuth_end_reached = 0;    //!< Has the final azimuth been reached
  char elevation_end_reached = 0;  //!< Has the final elevation been reached
  
  // unpack configuration data
  float sample_period_MS = 1000/((float)c.frequency); 
  unsigned char LCD_heading = c.control & LCD_HEADING_MASK;
  unsigned char LCD_elevation = (c.control & LCD_ELEVATION_MASK)>>2;
  float azimuth_step = ((float)c.headStep)/10;
  float elevation_step = ((float)c.elevStep)/10;	
  
  // initialise values for minimum range
  range_min = 999;
  range_min_azimuth = 0;
  range_min_elevation = 0;
  
  // prepare the magnetometer for use
  prepareMag();
  
  // prepare the gyro for use, moving the PTU to the starting position
  prepareGyro(c.headStart);   
  
  // step through the azimuth range
  azimuth = c.headStart;
  
  while (azimuth_end_reached==0){
  
    // set the servos to the current azimuth
    setAzimuth(azimuth);
    delay1(100);
    
    // sample the azimuth reading
    map_gyro_azimuth = returnGyroAzimuth();
    map_mag_azimuth = returnMagAzimuth();
    
    // set the servo to the minimum elevation
    elevation = c.elevStart;
    setElevation(elevation);
    delay1(100);
    
    // step through the elevation range
     while(elevation_end_reached==0){
      setElevation(elevation);
      delay1(50);
      
      
      // sample elevation
      map_gyro_elevation = returnGyroElevation();
      map_acc_elevation = returnAccElevation();
      sum = 0;
      // take laser samples
      for(i=0;i<c.samples;i++){
        sum += laserRange();
        delay1((unsigned)sample_period_MS);                             // delay according to sample frequency
      }
      
      map_laser_range = sum/(c.samples);                                // take average of samples (basic filter)
      
      // calculate x,y,z using chosen data
      // choose azimuth data
      if(c.headCalc == SERVO_CODE){
        calc_azimuth = azimuth;
      }else if(c.headCalc == GYRO_CODE){
        calc_azimuth = map_gyro_azimuth;
      } else if(c.headCalc == MAG_CODE){
        calc_azimuth = map_mag_azimuth;
      }
      // choose elevation data
      if(c.elevCalc == SERVO_CODE){
        calc_elevation = elevation;
      }else if(c.elevCalc == GYRO_CODE){
        calc_elevation = map_gyro_elevation;
      } else if(c.elevCalc == ACC_CODE){
        calc_elevation = map_acc_elevation;
      }
      // determine x,y,z
      x_data = returnX(map_laser_range,calc_elevation,calc_azimuth);
      y_data = returnY(map_laser_range,calc_elevation,calc_azimuth);
      z_data = returnZ(map_laser_range,calc_elevation);
      addValuePacket('P',i,x_data,y_data,z_data);     // send position packet
      addValuePacket('O',i,(float)calc_azimuth,(float)calc_elevation,(float)map_laser_range); // send orientation packet
      update_min_values(calc_azimuth,calc_elevation,map_laser_range); // update minimum range data if applicable
        
      // at the first orientation, start displaying on the 7segs and LCD
      if(azimuth==c.headStart && elevation==c.elevStart){
        init_7seg();
        initTC4();
        LCD_clear();
        printLCD(1,0,"Azimuth:");
         printLCD(2,0,"Elevation:");
      }
        
      // print to the LCD
      // print chosen azimuth data
      if(LCD_heading == SERVO_CODE){
        float_to_str(azimuth,azimuth_string);
      }else if(LCD_heading == GYRO_CODE){
        float_to_str(map_gyro_azimuth,azimuth_string); 
      }else if(LCD_heading == MAG_CODE){
        float_to_str(map_mag_azimuth,azimuth_string);
      }
      printLCD(1,10,azimuth_string);
      // print chosen elevation data
      if(LCD_elevation == SERVO_CODE){
        float_to_str(elevation,elevation_string);
      }else if(LCD_elevation == GYRO_CODE){
        float_to_str(map_gyro_elevation,elevation_string);
      }else if(LCD_elevation == ACC_CODE){
          float_to_str(map_acc_elevation,elevation_string);
      }
      printLCD(2,10,elevation_string);    
      
          
      
      // check if elevation end has been reached
      if((float)elevation>(float)c.elevEnd-(elevation_step)/2 && (float)elevation<(float)c.elevEnd+(elevation_step)/2){
      elevation_end_reached=1;
      }
      
      // increase/decrease the elevation by the step change
      if(c.elevStart<=c.elevEnd){
        elevation += elevation_step;
      }
      else{
        elevation -= elevation_step;
      }
            
    }
    
    // check if azimuth end has been reached
    if((float)azimuth>(float)c.headEnd-(azimuth_step)/2 && (float)azimuth<(float)c.headEnd+(azimuth_step)/2){
      azimuth_end_reached=1;
    }
    
    // reset elevation end reached
    elevation_end_reached = 0;
    // return to an elevation of zero
    setElevation(0);
    delay1(100);
    
    // increase/decrease the azimuth by the step change
    if(c.headStart<=c.headEnd){
      azimuth += azimuth_step;
    }
    else{
      azimuth -= azimuth_step;
    }
    
  }
  
  // send minimum range packets and update board display with the same data
  addValuePacket('R',i,range_min_azimuth,range_min_elevation,range_min);
  map_laser_range = range_min; 
  float_to_str(range_min_azimuth,azimuth_string);
  printLCD(1,10,azimuth_string);
  float_to_str(range_min_elevation,elevation_string);
  printLCD(2,10,elevation_string);
   
}

/// checks if there is a new nearest objects and stores data about the object's location
void update_min_values(float azimuth,float elevation,float range){
  if(fabs(range)<fabs(range_min)){
    range_min = range;
    range_min_azimuth = azimuth;
    range_min_elevation = elevation;
  }
}
  