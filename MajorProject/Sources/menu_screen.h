#ifndef MENU_SCREEN_H
#define MENU_SCREEN_H
  
  typedef enum { false, true } bool;   //!< define a boolean type

  void initMenu(void);
  void refreshMenu(void);

  void startup_menu(void);
  void keycheck(void);
  void menu_shift(void);
  
  void m_headingStart(void);
  void m_headingEnd(void);
  void m_elevationStart(void);
  void m_elevationEnd(void);
  void m_headingStep(void);
  void m_elevationStep(void);
  void m_samples(void);
  void m_frequency(void);
  void m_lcdHeading(void);
  void m_lcdElev(void);
  void m_plotting(void);
  void m_calcHeading(void);
  void m_calcElevation(void);
  
  void int_to_str(int,unsigned char*); 

#endif