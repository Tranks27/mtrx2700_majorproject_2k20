#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "keypad.h"

/*!
  * \file keypad.c
  * File that contains functions for operating keypad
*/

static const unsigned char rowCode[4] = {0x10, 0x20, 0x40, 0x80}; //!< lookup table for rows to scan
static const unsigned char colCode[4] = {0x01, 0x02, 0x04, 0x08}; //!< lookup table for cols to check
  
/** \brief Lookup table for ascii value of keys. \n
* 1,2,3,A \n 
* 4,5,6,B \n
* 7,8,9,C \n
* +,0,-,D 
*/
static const unsigned char keyCode[4][4] = {{0x31, 0x32, 0x33, 0x41},  
                                     {0x34, 0x35, 0x36, 0x42},  
                                     {0x37, 0x38, 0x39, 0x43},  
                                     {0x2B, 0x30, 0x2D, 0x44}   
                                    }; 

/// Initialise the keypad for use                               
void init_keypad(void){
  DDRA = 0xF0;    // set PA0-3 as input and PA4-5 as output
  
}

/// Detects input char from the keypad
unsigned char find_keyPressed(void){
  unsigned int currentRow;
  unsigned char key;
  unsigned int debounce_delay;
  unsigned char keyInput;
  
  // Go through each row of keypad
  for(currentRow = 0; currentRow <4; currentRow++){
  
     key = keyScanner(currentRow);          // first check for key input
     for(debounce_delay=0; debounce_delay < 250; debounce_delay++); // Small delay for debouncing
       if(key == keyScanner(currentRow)){   // second check for key input confirmation
          if(key!='\0'){
            keyInput = key;
          }
       } 
  }
  
  return keyInput;
}

/// \brief Sub function of find_keyPressed. Scan each row and return if any key pressed
unsigned char keyScanner(unsigned int currentRow){
  PORTA = rowCode[currentRow];   // Set PA4-7 to be high one at a time
  
  if(PORTA & colCode[0]){            //first key in row
     return keyCode[currentRow][0];
  }else if(PORTA & colCode[1]){       //second key in row
     return keyCode[currentRow][1];
  }else if(PORTA & colCode[2]){       //third key in row
     return keyCode[currentRow][2];
  }else if(PORTA & colCode[3]){       //fourth key in row
     return keyCode[currentRow][3];
  }else{
     return '\0'; // no key pressed, return NULL
  } 
}