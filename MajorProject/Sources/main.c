
/*!
  * \file main.c
  * Main file for mapping program
  * Project is built using minimal startup code and small memory model
  * IEEE32 float standard
  * Sensors implemented:
  * - Gryro L2g2400D  (3 axis )
  * - Accelerometer  ADXL345
  * - Magnetometer HM5883
  * - Laser Lidarlight

  * System operation at 24MHz, Baud Rate 9600
  * - Prescaler 1
  * - Timer 1 is used for laser sampling
  * - Timer 2 is used for gyro sampling
  * - Timer 4 is used for LED refreshing
  * - Timer 6 is used for sampling time
  * - Timer 7 is used for iic drivers
*/

/*!
  * \mainpage 3 Dimensional Mapping System Documentation
  * \authors Group 20 - Nathan Dugdale, Joel Donnelly, Lily Couper, Tranks Naing
  * \section intro_sec Introduction
  * This software has been written to generate a three-dimensional point cloud
  * using the Dragon12 board, as well as the hardware module. This documentation documents all source code
  * used in this project, including the mapping functions, the initialisation functions, and finally functions
  * to allow both on board and on PC configuration. For this code to function as expected, the user must have a PC
  * with 'Codewarrior' installed, as well as MATLAB. Without these two applications, this code will not run, and
  * the hardware will not communicate with the software. In addition to this, the user must read the user manual,
  * to have an adequate grasp upon operation and configuration of the device. Finally, all code in this HTML file is
  * documented, with the source code displayed. The user should go to 'Files' and then select the file they wish to view.
*/

#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "iic.h"
#include "pll.h"
#include "servo.h"
#include "laser.h"
#include "imu.h"
#include "rawdata.h"
#include "delay.h"
#include "map.h"
#include "serial.h"
#include "config.h"
#include "display.h"
#include "keypad.h"
#include "menu_screen.h"
#include "testing.h"
#include <math.h>
#include "l3g4200.h"  // register's definitions    ( not used by ed )

extern char programState;       //!< state of program - either 'C' for configuration or 'S' for scanning
extern config mergedConfigs;    //!< configuration settings used for mapping routine

/// Main function. Run this before launching the MATLAB app
void main(void) {

  PLL_Init();      // make sure we are running at 24 Mhz
  initPWM();       // initialise PWM for servos
  initMenu();      // prepare LCD for menu display
  init_serial();   // initialise SCI

  Init_TC6();           // initialise delay timer
  initTC1();            // initialise TC1 for laser pulse-width reading

  EnableInterrupts;     // enable interrupts

  transmitterOFF();     // start in receiving mode to receive instruction from PC
  receiverON();

  init_LCD();           // initialise the LCD
  init_keypad();        // initialise the keypad

  iicinit();            // initialise iic bus

  gyro_init();          // l3g4200 setup
  accel_init();         // adxl345 setup
  magnet_init();        // hm5883 setup

  /* Testing
  Functions for the demonstration of devices. Uncomment one function at a time
  in order to demonstrate component. Ensure all test functions are commented
  before running the program for mapping.
  */
  //servoAzimuthTest();
  //servoElevationTest();
  //gyroTest();
  //magTest();
  //accTest();
  //laserTest();

  startup_menu();   // enter configuration menu

  // await start signal from PC or keypad
  while(programState!='S'){
    startup_menu();   // stay in configuration mode
  }

  // print message
  LCD_clear();
  printLCD(1,0,"Mapping Now");

  // map the room using configured values
  mapRoom(mergedConfigs);

  // loop endlessly
  while(1) {
  }

}




//   ******************  END Main   *****************
