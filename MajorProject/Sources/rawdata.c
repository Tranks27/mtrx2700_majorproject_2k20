
/*!
  * \file rawdata.c
  * File that contains functions to obtain raw data from the IMU
  * \author Eduardo Nebot
*/

#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "iic.h"
#include "rawdata.h"
#include "l3g4200.h"  // register's definitions    ( not used by ed )

/// Function to initialise the magnetometer
void magnet_init(void){

  int  res1;
  res1=iicstart(magnet_wr);
  res1=iictransmit(HM5883_MODE_REG );  //
  res1=iictransmit(0x00 );
  iicstop();

}

/// Function to test the magnetometer
void magnet_test(void){

}

/// Function to obtain raw data from the magnetometer
void hm5883_getrawdata(int *mxraw, int *myraw, int *mzraw){

 uint8_t i = 0;
 uint8_t buff[6];
 int res1;

 res1=iicstart(magnet_wr);
 res1=iictransmit(HM5883_DATAX0 );
 res1= iicrestart(magnet_rd);
 iicswrcv();

 for(i=0; i<4  ;i++) {
  buff[i]=iicreceive();
 }

 buff[i]= iicreceivem1();
 buff[i+1]= iicreceivelast();

	*mxraw = ((buff[0] << 8) | buff[1]);
	*myraw = ((buff[2] << 8) | buff[3]);
	*mzraw = ((buff[4] << 8) | buff[5]);
}

/// Function to test the accelerometer
void accel_test(void){}

/// Function to initialise the accelerometer
void accel_init (void){

 int  res1;

 res1=iicstart(accel_wr);
 res1=iictransmit(ADXL345_POWER_CTL );  //
 res1=iictransmit(0x08 );

 res1=iictransmit(ADXL345_DATA_FORMAT );  // ;
 res1=iictransmit(0x08 );

 iicstop();
}

/// Function to obtain raw data from the accelerometer
void adxl345_getrawdata(int *axraw, int *ayraw, int *azraw){

 uint8_t i = 0;
 uint8_t buff[6];
 int res1;

 res1=iicstart(accel_wr);
 res1=iictransmit(ADXL345_DATAX0 );
 res1= iicrestart(accel_rd);
 iicswrcv();

 for(i=0; i<4  ;i++) {
  buff[i]=iicreceive();
 }

 buff[i]= iicreceivem1();
 buff[i+1]= iicreceivelast();

	*axraw = ((buff[1] << 8) | buff[0]);
	*ayraw = ((buff[3] << 8) | buff[2]);
	*azraw = ((buff[5] << 8) | buff[4]);
}


/// Test the presence of the gyroscope - should get 211 on return
void gyro_test(void) {
 int res1,who;

 res1=iicstart(0xD2);
 res1=iictransmit(L3G4200D_WHO_AM_I);

 res1=iicrestart(0xD3);
 who=iicreceiveone();
 //who=who & 0x00ff;     Debugging  info
 //PORTB=  who ;

}


 /// Function to initialise the gyroscope
 void gyro_init (void) {

 int  res1;

 res1=iicstart(gyro_wr);
 res1=iictransmit(L3G4200D_CTRL_REG1 );  // ; 100hz, 12.5Hz, Power up
 res1=iictransmit(0x0f );
 iicstop();
 }


/// Function to get a set of gyro data - Eduardo Nebot,30 July 2015
void l3g4200d_getrawdata(int *gxraw, int *gyraw, int *gzraw) {
 	uint8_t i = 0;
	uint8_t buff[6];
	int res1;

   res1=iicstart(gyro_wr);
   res1=iictransmit(L3G4200D_OUT_XYZ_CONT );
   res1= iicrestart(gyro_rd);

 iicswrcv();

 for(i=0; i<4  ;i++) {
  buff[i]=iicreceive();
 }

buff[i]= iicreceivem1();
buff[i+1]= iicreceivelast();

	*gxraw = ((buff[1] << 8) | buff[0]);
	*gyraw = ((buff[3] << 8) | buff[2]);
	*gzraw = ((buff[5] << 8) | buff[4]);
}
