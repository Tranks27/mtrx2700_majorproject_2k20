var searchData=
[
  ['headcalc_172',['headCalc',['../structs__config.html#aab8f39f5f98c4f23a3fa0b0cf590d5de',1,'s_config']]],
  ['headend_173',['headEnd',['../structs__config.html#aea595e52055fc7997bd9f859f63b1467',1,'s_config']]],
  ['headstart_174',['headStart',['../structs__config.html#aec75d53970be04ee04cd62a75fdd5d0f',1,'s_config']]],
  ['headstep_175',['headStep',['../structs__config.html#a0235680966d71e4902efb3c558094099',1,'s_config']]],
  ['hm5883_5fdatax0_176',['HM5883_DATAX0',['../rawdata_8h.html#a57347e985160ca4ee83f8668c495f0ce',1,'rawdata.h']]],
  ['hm5883_5fgetrawdata_177',['hm5883_getrawdata',['../rawdata_8c.html#af59dedc0ceee4d389b7797e67006f82f',1,'hm5883_getrawdata(int *mxraw, int *myraw, int *mzraw):&#160;rawdata.c'],['../rawdata_8h.html#af59dedc0ceee4d389b7797e67006f82f',1,'hm5883_getrawdata(int *mxraw, int *myraw, int *mzraw):&#160;rawdata.c']]],
  ['hm5883_5fmode_5freg_178',['HM5883_MODE_REG',['../rawdata_8h.html#ac65b9eb2b41c22c0ce6d0c6996c9eda1',1,'rawdata.h']]]
];
