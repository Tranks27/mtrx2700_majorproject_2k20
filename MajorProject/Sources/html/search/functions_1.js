var searchData=
[
  ['accel_5finit_478',['accel_init',['../rawdata_8c.html#a2d93ea48e89b904e6a095a10fdae25ea',1,'accel_init(void):&#160;rawdata.c'],['../rawdata_8h.html#a2d93ea48e89b904e6a095a10fdae25ea',1,'accel_init(void):&#160;rawdata.c']]],
  ['accel_5ftest_479',['accel_test',['../rawdata_8c.html#a4394320be6884ae20a28233925555390',1,'accel_test(void):&#160;rawdata.c'],['../rawdata_8h.html#a4394320be6884ae20a28233925555390',1,'accel_test(void):&#160;rawdata.c']]],
  ['accelevation_480',['accElevation',['../imu_8c.html#a5cca5ba61ab30ab392a086e2e8504568',1,'accElevation(int ax, int az):&#160;imu.c'],['../imu_8h.html#a084de95caa1c598675a8fe76857f46b6',1,'accElevation(int, int):&#160;imu.c']]],
  ['acctest_481',['accTest',['../testing_8c.html#ad15c110f6182e3f523ef5124f40d5743',1,'accTest(void):&#160;testing.c'],['../testing_8h.html#ad15c110f6182e3f523ef5124f40d5743',1,'accTest(void):&#160;testing.c']]],
  ['actioninputpacket_482',['actionInputPacket',['../serial_8c.html#ab1065590fdf6a3e0c3b7c25ee34eb402',1,'actionInputPacket(void):&#160;serial.c'],['../serial_8h.html#ab1065590fdf6a3e0c3b7c25ee34eb402',1,'actionInputPacket(void):&#160;serial.c']]],
  ['addconfigpacket_483',['addConfigPacket',['../serial_8c.html#ae7f001dda8becaca72d3ad7deb3ec233',1,'addConfigPacket(config c):&#160;serial.c'],['../serial_8h.html#ae7f001dda8becaca72d3ad7deb3ec233',1,'addConfigPacket(config c):&#160;serial.c']]],
  ['addpacket_484',['addPacket',['../serial_8c.html#a4054b351b602345aeff2bd3ca753c69d',1,'addPacket(packet new_packet):&#160;serial.c'],['../serial_8h.html#a4054b351b602345aeff2bd3ca753c69d',1,'addPacket(packet new_packet):&#160;serial.c']]],
  ['addvaluepacket_485',['addValuePacket',['../serial_8c.html#a6de35c410fdf121259be02083196befe',1,'addValuePacket(char type, int groupID, float f0, float f1, float f2):&#160;serial.c'],['../serial_8h.html#a07721344113e29a80e456683e5d9637a',1,'addValuePacket(char, int, float, float, float):&#160;serial.c']]],
  ['adxl345_5fgetrawdata_486',['adxl345_getrawdata',['../rawdata_8c.html#a20f887795d533752ed639f6a6ac4bb82',1,'adxl345_getrawdata(int *axraw, int *ayraw, int *azraw):&#160;rawdata.c'],['../rawdata_8h.html#a20f887795d533752ed639f6a6ac4bb82',1,'adxl345_getrawdata(int *axraw, int *ayraw, int *azraw):&#160;rawdata.c']]],
  ['angulardisplacement_487',['angularDisplacement',['../imu_8c.html#a0e66e754384da33988d0c02de824184f',1,'angularDisplacement(int angularVelocity, float offset, float samplePeriod):&#160;imu.c'],['../imu_8h.html#a939d539812ca1aa3b3d9f65857e76889',1,'angularDisplacement(int, float, float):&#160;imu.c']]]
];
