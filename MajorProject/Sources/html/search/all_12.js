var searchData=
[
  ['s_5fconfig_359',['s_config',['../structs__config.html',1,'']]],
  ['sample_5ffreq_5fmax_360',['SAMPLE_FREQ_MAX',['../config_8h.html#ad203445ffea8d767799502f4e2ea2e59',1,'config.h']]],
  ['sample_5ffreq_5fmin_361',['SAMPLE_FREQ_MIN',['../config_8h.html#a90157f1c65bc95ba71f833c21cb7146b',1,'config.h']]],
  ['samples_362',['samples',['../structs__config.html#a3c7d91ac0d3890250ecce76fc32f8c52',1,'s_config']]],
  ['sci1_2ec_363',['sci1.c',['../sci1_8c.html',1,'']]],
  ['sci1_2eh_364',['sci1.h',['../sci1_8h.html',1,'']]],
  ['sci1_5finchar_365',['SCI1_InChar',['../sci1_8c.html#a0a29d5adbe4026a67dd92ef981fb5050',1,'SCI1_InChar(void):&#160;sci1.c'],['../sci1_8h.html#a0a29d5adbe4026a67dd92ef981fb5050',1,'SCI1_InChar(void):&#160;sci1.c']]],
  ['sci1_5finit_366',['SCI1_Init',['../sci1_8c.html#adc1becdb9176287d66fbd1ccea1e6ae3',1,'SCI1_Init(unsigned short baudRate):&#160;sci1.c'],['../sci1_8h.html#adc1becdb9176287d66fbd1ccea1e6ae3',1,'SCI1_Init(unsigned short baudRate):&#160;sci1.c']]],
  ['sci1_5finsdec_367',['SCI1_InSDec',['../sci1_8c.html#aed231a2448e931bccaabc32d8648acdf',1,'SCI1_InSDec(void):&#160;sci1.c'],['../sci1_8h.html#aed231a2448e931bccaabc32d8648acdf',1,'SCI1_InSDec(void):&#160;sci1.c']]],
  ['sci1_5finsldec_368',['SCI1_InSLDec',['../sci1_8c.html#a277702bee8ff06b73b144bd14bae2b25',1,'SCI1_InSLDec(void):&#160;sci1.c'],['../sci1_8h.html#a277702bee8ff06b73b144bd14bae2b25',1,'SCI1_InSLDec(void):&#160;sci1.c']]],
  ['sci1_5finstatus_369',['SCI1_InStatus',['../sci1_8c.html#a46c2afa059ff4bbc363a2b1db8aeb10f',1,'SCI1_InStatus(void):&#160;sci1.c'],['../sci1_8h.html#a46c2afa059ff4bbc363a2b1db8aeb10f',1,'SCI1_InStatus(void):&#160;sci1.c']]],
  ['sci1_5finstring_370',['SCI1_InString',['../sci1_8c.html#a8f6499b1354435b9cdbf31c24fa000bd',1,'SCI1_InString(char *string, unsigned short max):&#160;sci1.c'],['../sci1_8h.html#ac7ca9f8ad61aa7c81f431b7cebe0dd0c',1,'SCI1_InString(char *, unsigned short):&#160;sci1.c']]],
  ['sci1_5finudec_371',['SCI1_InUDec',['../sci1_8c.html#a62dfc81157684e46fd323a19cd1378be',1,'SCI1_InUDec(void):&#160;sci1.c'],['../sci1_8h.html#a62dfc81157684e46fd323a19cd1378be',1,'SCI1_InUDec(void):&#160;sci1.c']]],
  ['sci1_5finuhex_372',['SCI1_InUHex',['../sci1_8c.html#a2c77c4693293a31f42e2139035434443',1,'SCI1_InUHex(void):&#160;sci1.c'],['../sci1_8h.html#a2c77c4693293a31f42e2139035434443',1,'SCI1_InUHex(void):&#160;sci1.c']]],
  ['sci1_5finuldec_373',['SCI1_InULDec',['../sci1_8c.html#a802ea62764db38f2190461e9b2091399',1,'SCI1_InULDec(void):&#160;sci1.c'],['../sci1_8h.html#a802ea62764db38f2190461e9b2091399',1,'SCI1_InULDec(void):&#160;sci1.c']]],
  ['sci1_5fisr_374',['sci1_isr',['../serial_8c.html#a01abb903de1f0ec6647d685843aa7f34',1,'serial.c']]],
  ['sci1_5foutchar_375',['SCI1_OutChar',['../sci1_8c.html#a497eef85675e1ef92dc2b53cf29a6da9',1,'SCI1_OutChar(char data):&#160;sci1.c'],['../sci1_8h.html#ad17a8601a6e98b0f67a8b25cc2487d35',1,'SCI1_OutChar(char):&#160;sci1.c']]],
  ['sci1_5foutstatus_376',['SCI1_OutStatus',['../sci1_8c.html#a49a17047a2c80493f34103bf03d690e3',1,'SCI1_OutStatus(void):&#160;sci1.c'],['../sci1_8h.html#a49a17047a2c80493f34103bf03d690e3',1,'SCI1_OutStatus(void):&#160;sci1.c']]],
  ['sci1_5foutstring_377',['SCI1_OutString',['../sci1_8c.html#a0174d2c42c39d7cf956af7ad60dd4fe7',1,'SCI1_OutString(char *pt):&#160;sci1.c'],['../sci1_8h.html#a0174d2c42c39d7cf956af7ad60dd4fe7',1,'SCI1_OutString(char *pt):&#160;sci1.c']]],
  ['sci1_5foutudec_378',['SCI1_OutUDec',['../sci1_8c.html#aa885aede13049b7ad6797bbe6e405997',1,'SCI1_OutUDec(unsigned short n):&#160;sci1.c'],['../sci1_8h.html#aa7005fefe976671ad2a03fd5154bc1c4',1,'SCI1_OutUDec(unsigned short):&#160;sci1.c']]],
  ['sci1_5foutuhex_379',['SCI1_OutUHex',['../sci1_8c.html#a26f6096a3004e3b645f9d49e2e065a78',1,'SCI1_OutUHex(unsigned short number):&#160;sci1.c'],['../sci1_8h.html#a0aaa5a04084b146d9d38259d01091bb7',1,'SCI1_OutUHex(unsigned short):&#160;sci1.c']]],
  ['sci_5freceiving_380',['SCI_receiving',['../serial_8c.html#a8980ba920f14968ff1135ef9672456f1',1,'serial.c']]],
  ['sci_5ftransmitting_381',['SCI_transmitting',['../serial_8c.html#af64812ccd57eaf9a879b6216e390751d',1,'serial.c']]],
  ['secondaryconfigs_382',['secondaryConfigs',['../config_8c.html#abb7941d82b588c31396706ab1f5d0154',1,'secondaryConfigs():&#160;config.c'],['../menu__screen_8c.html#abb7941d82b588c31396706ab1f5d0154',1,'secondaryConfigs():&#160;config.c'],['../serial_8c.html#abb7941d82b588c31396706ab1f5d0154',1,'secondaryConfigs():&#160;config.c']]],
  ['seg_5fdisplay_383',['SEG_DISPLAY',['../display_8c.html#a21e7a377e125b87bc0f4324b5fa7a23e',1,'display.c']]],
  ['seg_5fenable_384',['SEG_ENABLE',['../display_8c.html#a113ec854ab57ab7a632ee890d8bd1203',1,'display.c']]],
  ['serial_2ec_385',['serial.c',['../serial_8c.html',1,'']]],
  ['serial_2eh_386',['serial.h',['../serial_8h.html',1,'']]],
  ['servo_2ec_387',['servo.c',['../servo_8c.html',1,'']]],
  ['servo_2eh_388',['servo.h',['../servo_8h.html',1,'']]],
  ['servo_5fcode_389',['SERVO_CODE',['../config_8h.html#a3cee0463a29e3de012684601d88408b9',1,'config.h']]],
  ['servoazimuthtest_390',['servoAzimuthTest',['../testing_8c.html#a7e369627da7b574714b428664de31962',1,'servoAzimuthTest(void):&#160;testing.c'],['../testing_8h.html#a7e369627da7b574714b428664de31962',1,'servoAzimuthTest(void):&#160;testing.c']]],
  ['servoelevationtest_391',['servoElevationTest',['../testing_8c.html#abe14715afacca8b51c78fe70c8c76cdd',1,'servoElevationTest(void):&#160;testing.c'],['../testing_8h.html#abe14715afacca8b51c78fe70c8c76cdd',1,'servoElevationTest(void):&#160;testing.c']]],
  ['setalarm_392',['setAlarm',['../iic_8c.html#a117c6e0a8f04e3d7807f78cb7287268b',1,'iic.c']]],
  ['setalarm1_393',['setAlarm1',['../delay_8c.html#a7f1937e8d3e50832e59ba221ce34f8f1',1,'setAlarm1(uint16_t msDelay1):&#160;delay.c'],['../delay_8h.html#a7f1937e8d3e50832e59ba221ce34f8f1',1,'setAlarm1(uint16_t msDelay1):&#160;delay.c']]],
  ['setazimuth_394',['setAzimuth',['../servo_8c.html#a1cd8996990ca9eeacde65af2b082044a',1,'setAzimuth(float degrees):&#160;servo.c'],['../servo_8h.html#a1cd8996990ca9eeacde65af2b082044a',1,'setAzimuth(float degrees):&#160;servo.c']]],
  ['setelevation_395',['setElevation',['../servo_8c.html#a7a9e7b7c761db4b8b5aeab6722a8a58b',1,'setElevation(float degrees):&#160;servo.c'],['../servo_8h.html#a7a9e7b7c761db4b8b5aeab6722a8a58b',1,'setElevation(float degrees):&#160;servo.c']]],
  ['sevenseg_5fisr_396',['sevenSeg_isr',['../display_8c.html#aa141bb3b67a0183d74c3a8b7e30f302e',1,'display.c']]],
  ['sp_397',['SP',['../sci1_8h.html#aecd69d9a67487cc45c38eb184c50538a',1,'sci1.h']]],
  ['start_398',['START',['../menu__screen_8c.html#a991094a2cc72be82f14668a78f8c2cc6',1,'START():&#160;menu_screen.c'],['../serial_8c.html#ac169e9ffa53b0e66f7f30d192d44643f',1,'START():&#160;menu_screen.c']]],
  ['start12_2ec_399',['Start12.c',['../_start12_8c.html',1,'']]],
  ['startup_5fmenu_400',['startup_menu',['../menu__screen_8c.html#aa4eda8c04bd95594489f7e7247dea1b6',1,'startup_menu(void):&#160;menu_screen.c'],['../menu__screen_8h.html#aa4eda8c04bd95594489f7e7247dea1b6',1,'startup_menu(void):&#160;menu_screen.c']]],
  ['state_401',['state',['../structconfig__packet.html#ad647b28c34dcc328798f5f1ab1d8e07b',1,'config_packet']]],
  ['step_5fmax_402',['STEP_MAX',['../config_8h.html#afb1629f78e781dc38b5ded85f69928eb',1,'config.h']]],
  ['step_5fmin_403',['STEP_MIN',['../config_8h.html#a560eb1ca0807e463d7f7dddadf4ab339',1,'config.h']]]
];
