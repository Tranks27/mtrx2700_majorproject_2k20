var searchData=
[
  ['pll_5finit_553',['PLL_Init',['../pll_8c.html#a53f1e60b4a581dabc1a6fd9447a3eb15',1,'PLL_Init(void):&#160;pll.c'],['../pll_8h.html#a53f1e60b4a581dabc1a6fd9447a3eb15',1,'PLL_Init(void):&#160;pll.c']]],
  ['preparegyro_554',['prepareGyro',['../imu_8c.html#a76f60f6a3ee53b27340dfd13a65f80da',1,'prepareGyro(int azimuthStart):&#160;imu.c'],['../imu_8h.html#aa6a9bce11f2519ee7fc281d923c31ec8',1,'prepareGyro(int):&#160;imu.c']]],
  ['preparemag_555',['prepareMag',['../imu_8c.html#a4fc01c57d86f69bdb119f89d9881c548',1,'prepareMag(void):&#160;imu.c'],['../imu_8h.html#a4fc01c57d86f69bdb119f89d9881c548',1,'prepareMag(void):&#160;imu.c']]],
  ['print_5f7seg_556',['print_7seg',['../display_8c.html#a59e317e5dcfe16c4a878350d43c2a949',1,'print_7seg(float value):&#160;display.c'],['../display_8h.html#a8e14827174b26aee4dce2a25614be61f',1,'print_7seg(float):&#160;display.c']]],
  ['printlcd_557',['printLCD',['../display_8c.html#a5c48660b64aaf0ac57ee6de533846df4',1,'printLCD(unsigned int row, unsigned int col, unsigned char *buffer):&#160;display.c'],['../display_8h.html#ac352676ab9b3effb081b50c194cc6a46',1,'printLCD(unsigned int, unsigned int, unsigned char *):&#160;display.c']]]
];
