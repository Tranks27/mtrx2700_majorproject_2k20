var searchData=
[
  ['u_5fpacket_419',['u_packet',['../unionu__packet.html',1,'']]],
  ['uint16_5ft_420',['uint16_t',['../delay_8h.html#a1f1825b69244eb3ad2c7165ddc99c956',1,'uint16_t():&#160;delay.h'],['../iic_8c.html#a1f1825b69244eb3ad2c7165ddc99c956',1,'uint16_t():&#160;iic.c'],['../iic_8h.html#a1f1825b69244eb3ad2c7165ddc99c956',1,'uint16_t():&#160;iic.h']]],
  ['uint8_5ft_421',['uint8_t',['../delay_8h.html#aba7bc1797add20fe3efdf37ced1182c5',1,'uint8_t():&#160;delay.h'],['../iic_8c.html#aba7bc1797add20fe3efdf37ced1182c5',1,'uint8_t():&#160;iic.c'],['../iic_8h.html#aba7bc1797add20fe3efdf37ced1182c5',1,'uint8_t():&#160;iic.h']]],
  ['update_5fmin_5fvalues_422',['update_min_values',['../map_8c.html#a7186a8a88cea03f6fa4780d49dbba70c',1,'update_min_values(float azimuth, float elevation, float range):&#160;map.c'],['../map_8h.html#a3a7cf5d5b5b009e5e20d9b24363f763f',1,'update_min_values(float, float, float):&#160;map.c']]],
  ['updateconfigs_423',['updateConfigs',['../config_8c.html#af68d1ddd4841ee43e98d508c1a99dbe0',1,'updateConfigs(config *merged, config *primary, config *secondary):&#160;config.c'],['../config_8h.html#ae8f8804af493eb74ec797ec54ce0fddb',1,'updateConfigs(config *, config *, config *):&#160;config.c']]],
  ['use_5fseveral_5fpages_424',['USE_SEVERAL_PAGES',['../datapage_8c.html#a004e592d2b8177b83331ebba0855a07f',1,'datapage.c']]]
];
