var searchData=
[
  ['samples_693',['samples',['../structs__config.html#a3c7d91ac0d3890250ecce76fc32f8c52',1,'s_config']]],
  ['sci_5freceiving_694',['SCI_receiving',['../serial_8c.html#a8980ba920f14968ff1135ef9672456f1',1,'serial.c']]],
  ['sci_5ftransmitting_695',['SCI_transmitting',['../serial_8c.html#af64812ccd57eaf9a879b6216e390751d',1,'serial.c']]],
  ['secondaryconfigs_696',['secondaryConfigs',['../config_8c.html#abb7941d82b588c31396706ab1f5d0154',1,'secondaryConfigs():&#160;config.c'],['../menu__screen_8c.html#abb7941d82b588c31396706ab1f5d0154',1,'secondaryConfigs():&#160;config.c'],['../serial_8c.html#abb7941d82b588c31396706ab1f5d0154',1,'secondaryConfigs():&#160;config.c']]],
  ['seg_5fdisplay_697',['SEG_DISPLAY',['../display_8c.html#a21e7a377e125b87bc0f4324b5fa7a23e',1,'display.c']]],
  ['seg_5fenable_698',['SEG_ENABLE',['../display_8c.html#a113ec854ab57ab7a632ee890d8bd1203',1,'display.c']]],
  ['start_699',['START',['../menu__screen_8c.html#a991094a2cc72be82f14668a78f8c2cc6',1,'START():&#160;menu_screen.c'],['../serial_8c.html#ac169e9ffa53b0e66f7f30d192d44643f',1,'START():&#160;menu_screen.c']]],
  ['state_700',['state',['../structconfig__packet.html#ad647b28c34dcc328798f5f1ab1d8e07b',1,'config_packet']]]
];
