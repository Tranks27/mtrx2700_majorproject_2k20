var searchData=
[
  ['_5ffar_5fcopy_465',['_FAR_COPY',['../datapage_8c.html#a0c40ae57b0e28d34f2b2243d589035b9',1,'datapage.c']]],
  ['_5ffar_5fcopy_5frc_466',['_FAR_COPY_RC',['../datapage_8c.html#a9fbcbca5e063b08462e7c5fd95029c74',1,'datapage.c']]],
  ['_5fget_5fpage_5freg_467',['_GET_PAGE_REG',['../datapage_8c.html#a051273ce1767c4e67a8ae333ea6aec2e',1,'datapage.c']]],
  ['_5fload_5ffar_5f16_468',['_LOAD_FAR_16',['../datapage_8c.html#af222c8003364af363e288a1da14c9576',1,'datapage.c']]],
  ['_5fload_5ffar_5f24_469',['_LOAD_FAR_24',['../datapage_8c.html#a18db52fcd61a9397db596af55407053a',1,'datapage.c']]],
  ['_5fload_5ffar_5f32_470',['_LOAD_FAR_32',['../datapage_8c.html#a7f34edd31d71492df7291d5df2e3d30f',1,'datapage.c']]],
  ['_5fload_5ffar_5f8_471',['_LOAD_FAR_8',['../datapage_8c.html#a5840bd1a378294675da350b9b02e7b3f',1,'datapage.c']]],
  ['_5fset_5fpage_472',['_SET_PAGE',['../datapage_8c.html#ae8bede150b30359bc041c916f56310b3',1,'datapage.c']]],
  ['_5fstartup_473',['_Startup',['../_start12_8c.html#ab8265b54c7bdffffd90f3f868024a84d',1,'Start12.c']]],
  ['_5fstore_5ffar_5f16_474',['_STORE_FAR_16',['../datapage_8c.html#ae524262334be21641a99101b7a7e442a',1,'datapage.c']]],
  ['_5fstore_5ffar_5f24_475',['_STORE_FAR_24',['../datapage_8c.html#a7d19cb97eadfb2e1c0a1f174b3d2b0d5',1,'datapage.c']]],
  ['_5fstore_5ffar_5f32_476',['_STORE_FAR_32',['../datapage_8c.html#ad08fdae40b399d823b1ee110962b071b',1,'datapage.c']]],
  ['_5fstore_5ffar_5f8_477',['_STORE_FAR_8',['../datapage_8c.html#a6782c5190b9be688e3f9f704d36bef8b',1,'datapage.c']]]
];
