var searchData=
[
  ['readdata_558',['readData',['../serial_8c.html#ab1087f1c54245cd7a4d48a91fe025abf',1,'readData(void):&#160;serial.c'],['../serial_8h.html#ab1087f1c54245cd7a4d48a91fe025abf',1,'readData(void):&#160;serial.c']]],
  ['readpacket_559',['readPacket',['../serial_8c.html#aadd3ecde7f214dc7134b64afeb73ba7f',1,'readPacket(void):&#160;serial.c'],['../serial_8h.html#aadd3ecde7f214dc7134b64afeb73ba7f',1,'readPacket(void):&#160;serial.c']]],
  ['readpulsewidthcycles_560',['readPulseWidthCycles',['../laser_8c.html#a5b5ab926931d44b70cf6a6f9e61081dd',1,'readPulseWidthCycles(void):&#160;laser.c'],['../laser_8h.html#a5b5ab926931d44b70cf6a6f9e61081dd',1,'readPulseWidthCycles(void):&#160;laser.c']]],
  ['receiveroff_561',['receiverOFF',['../serial_8c.html#ae158bb3c512b5e87b950f0e6de4d5097',1,'receiverOFF(void):&#160;serial.c'],['../serial_8h.html#ae158bb3c512b5e87b950f0e6de4d5097',1,'receiverOFF(void):&#160;serial.c']]],
  ['receiveron_562',['receiverON',['../serial_8c.html#aec298f5f59497806cf31c58c5c4b38a3',1,'receiverON(void):&#160;serial.c'],['../serial_8h.html#aec298f5f59497806cf31c58c5c4b38a3',1,'receiverON(void):&#160;serial.c']]],
  ['refreshmenu_563',['refreshMenu',['../menu__screen_8c.html#a107efe916cebfaa0d2707f32e4fe5f03',1,'refreshMenu(void):&#160;menu_screen.c'],['../menu__screen_8h.html#a107efe916cebfaa0d2707f32e4fe5f03',1,'refreshMenu(void):&#160;menu_screen.c']]],
  ['returnaccelevation_564',['returnAccElevation',['../imu_8c.html#a90af6ae1aca32dcf5ab2a3682c243e43',1,'returnAccElevation(void):&#160;imu.c'],['../imu_8h.html#a90af6ae1aca32dcf5ab2a3682c243e43',1,'returnAccElevation(void):&#160;imu.c']]],
  ['returngyroazimuth_565',['returnGyroAzimuth',['../imu_8c.html#a3cd2d9396d12cb0ca45c50948637e794',1,'returnGyroAzimuth(void):&#160;imu.c'],['../imu_8h.html#a3cd2d9396d12cb0ca45c50948637e794',1,'returnGyroAzimuth(void):&#160;imu.c']]],
  ['returngyroelevation_566',['returnGyroElevation',['../imu_8c.html#ae6681e6dabb7800dc8106a485025d4f5',1,'returnGyroElevation(void):&#160;imu.c'],['../imu_8h.html#ae6681e6dabb7800dc8106a485025d4f5',1,'returnGyroElevation(void):&#160;imu.c']]],
  ['returnmagazimuth_567',['returnMagAzimuth',['../imu_8c.html#a30e0e138665068f4d235f77d7cd6063a',1,'returnMagAzimuth(void):&#160;imu.c'],['../imu_8h.html#a30e0e138665068f4d235f77d7cd6063a',1,'returnMagAzimuth(void):&#160;imu.c']]],
  ['returnx_568',['returnX',['../imu_8c.html#ac8a8dd9d36cee8ac14a5174325ee8124',1,'returnX(float range, float elevation, float azimuth):&#160;imu.c'],['../imu_8h.html#a36f5a6a60008e3cd5ca9480fc39412c1',1,'returnX(float, float, float):&#160;imu.c']]],
  ['returny_569',['returnY',['../imu_8c.html#a1f1de429903ff4b95d4bead25ae97488',1,'returnY(float range, float elevation, float azimuth):&#160;imu.c'],['../imu_8h.html#a71cc361fe18438c4982260f09fb43d54',1,'returnY(float, float, float):&#160;imu.c']]],
  ['returnz_570',['returnZ',['../imu_8c.html#ad6e9cd0796c6bf6f079d570a70d22a1c',1,'returnZ(float range, float elevation):&#160;imu.c'],['../imu_8h.html#a4ab23a2085705616e88db7ef743cb299',1,'returnZ(float, float):&#160;imu.c']]]
];
