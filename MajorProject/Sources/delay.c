/*!
  * \file delay.c
  * \author Eduardo Nebot

  * File that contains all delay functions.
*/

#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "iic.h"
#include "delay.h"

volatile uint8_t alarmSignaled1 = 0; //!< Flag set when alarm 1 signaled

volatile uint16_t currentTime1 = 0; //!< Variables private to timeout routines
uint16_t alarmTime1 = 0;
volatile uint8_t alarmSet1 = 0;

void setAlarm1(uint16_t msDelay1)
{
    alarmTime1 = currentTime1 + msDelay1;
    alarmSet1 = 1;
    alarmSignaled1 = 0;
}


void delay1(uint16_t msec)
{
    TC6 = TCNT + 24000; // Set initial time
    setAlarm1(msec);
    while(!alarmSignaled1) {};
}

/**
 * Interrupt(((0x10000-Vtimch7)/2)-1) void TC7_ISR(void){
// the line above is to make it portable between differen
// Freescale processors
// The symbols for each interrupt ( in this case Vtimch7 )'
// are defined in the provided variable definition file
// I am usign a much simpler definition ( vector number)
// that is easier to understand
*/

interrupt 14 void TC6_ISR(void) {

  TC6 =TCNT + 24000;   // interrupt every msec
  TFLG1=TFLG1 | TFLG1_C6F_MASK;
  currentTime1++;
    if (alarmSet1 && currentTime1 == alarmTime1)
    {
        alarmSignaled1 = 1;
        alarmSet1 = 0;
    }
   //PORTB=PORTB+1;        // count   (debugging)
}


void Init_TC6 (void) {

_asm SEI;

TSCR1=0x80;                   // timer enable
TSCR2=0x00;                   // prescaler 1
TIOS=TIOS | TIOS_IOS6_MASK;   // set TC6 for output compare function
TCTL1=0x40;
TIE=TIE | 0x40;

 _asm CLI;

}
