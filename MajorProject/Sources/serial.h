#ifndef SERIAL_H
#define SERIAL_H
#include "config.h"

/// definition of float packet 
typedef struct float_packet{
  float data[3];           //!< 3 floats
  int groupID;             //!< ID for data packet
  char pack_type;          //!< Type of packet
}flt_packet; // packet structs are formed in reverse order

/// definition of configuration packet
typedef struct config_packet{
  char state;  //!< either C (configuration) or S (scanning)
  char empty;  //!< placeholder, ensuring packets are equally sized
  config configData;  //!< configuration data
  char pack_type;     //!< Type of packet
}cfg_packet; // packet structs are formed in reverse order

/// definition of packet union
typedef union u_packet{
  unsigned char chunks[15];    //!< 15 bytes long
  flt_packet Pfloat;
  cfg_packet Pconfig;
}packet;


/*
packet data:
pack_type:  singe char to classify what data is being sent in packet
                - P = posititions, 3 floats of x,y,z
                - C = configs, 15 bytes worth of config data
                - O = orientation, 3 floats of azimuth, elevation, range
                - R = minimum range, 3 floats of azimuth, elevation, range
groupID:    single int (2 bytes) which acts as an ID for the sent data
*/

// declare function prototypes
void addPacket(packet new_packet);
void addConfigPacket(config c);
void addValuePacket(char,int,float,float,float);
void init_SCI(void);
void init_serial(void);
void receiverON(void);
void transmitterON(void);
void receiverOFF(void);
void transmitterOFF(void);
void transmitPacket(void);
void transmitData(unsigned char output);
void actionInputPacket(void);
void readPacket(void);
unsigned char readData(void);

// declare named constants
#define MAX_PACKETS 256            //!< Maximum packets allowed in the waitlist
#define CR 0x0D                    //!< ASCII value for Carriage Return
#define LF 0x0A                    //!< ASCII value for Line Feed

#endif