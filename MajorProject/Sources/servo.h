#ifndef SERVO_H
#define SERVO_H

void setAzimuth(float degrees);
void setElevation(float degrees);
void initPWM(void);

#define DEGREES_MAX 160     //!< Maximum servo angle
#define DEGREES_MIN 20      //!< Minimum servo angle

#endif