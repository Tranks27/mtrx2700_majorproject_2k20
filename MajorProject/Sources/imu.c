/*!
  * \file imu.c
  * File that contains all functions for position calculation, from data provided by the IMU.
*/

#include <hidef.h>            /* common defines and macros */
#include "derivative.h"       /* derivative-specific definitions */
#include "imu.h"
#include <math.h>
#include "servo.h"
#include "delay.h"
#include "rawdata.h"



static char buff[BUFF_SIZE]; //!< Buffer
static int gxraw[BUFF_SIZE]; //!< Raw x values from gyroscope
static int gyraw[BUFF_SIZE]; //!< Raw y values from gyroscope
static int gzraw[BUFF_SIZE];	//!< Raw z values from gyroscope
static int axraw[BUFF_SIZE]; //!< Raw x value from accelerometer
static int ayraw[BUFF_SIZE]; //!< Raw y value from accelerometer
static int azraw[BUFF_SIZE];  //!< Raw z value from accelerometer
static int mxraw[BUFF_SIZE]; //!< Raw x value from magnetometer
static int myraw[BUFF_SIZE]; //!< Raw y value from magnetometer
static int mzraw[BUFF_SIZE];  //!< Raw z value from magnetometer

static float gyro_elevation; //!< Value for elevation calculated by the gyroscope
static float gyro_azimuth; //!< Value for azimuth from the gyroscope
static float mag_heading90; //!< Value for heading from the magnetometer
static unsigned gyro_isr_count; //!< Gyro isr counter

/**
 * Finds the elevation of the PTU

 * Formula: elevation = (float)atan(((double)az-AZ_OFFSET)/((double)ax-AX_OFFSET))*(180/PI);
 * @param[in] angularVelocity, offset, samplePeriod
 * @param[out] elevation
*/
float accElevation(int ax,int az){

  float elevation = (float)atan(((double)az-AZ_OFFSET)/((double)ax-AX_OFFSET))*(180/PI);
  return elevation;
}

/**
 * Finds the angular displacement of the PTU

 * Formula: angularDisplacement = ((float)angularVelocity-offset)*samplePeriod
 * @param[in] angularVelocity, offset, samplePeriod
 * @param[out] angularDisplacement
*/
float angularDisplacement(int angularVelocity,float offset,float samplePeriod){

  float angularDisplacement = ((float)angularVelocity-offset)*samplePeriod;
  return angularDisplacement;
}

/**
 * Returns the value for the azimuth of the PTU given data from the magnetometer.

 * Formula: azimuth = 90-(double)heading90-atan2((double)mz,(double)my)*(180/PI);
 * @param[in] my, mz, heading90
 * @param[out] azimuth
*/
float magAzimuth(int my,int mz,float heading90){

  float azimuth = 90-(double)heading90-atan2((double)mz,(double)my)*(180/PI);

  // Account for sensor inaccuracies
  azimuth = 1.1*azimuth - 5.33;

  // If the azimuth is less than -180, add 360 degrees to it
  while(azimuth<-180){
    azimuth += 360;
  }
  return azimuth;
}

/**
 * Returns the value for the heading of the PTU using the magnetometer.

 * Formula: heading = -atan2((double)mz,(double)my)*(180/PI);
 * @param[in] my, mz
 * @param[out] heading
*/
float magHeading(int my,int mz){

  float heading = -atan2((double)mz,(double)my)*(180/PI);
  return heading;
}

/// Initialise the TC2 counter
void initTC2(void){
  gyro_isr_count = 0;       // Set isr counter to zero
  TSCR1 = 0x80;             // Timer enable
  TSCR2 = 0x00;             // Prescaler 1
  TIOS |= TIOS_IOS2_MASK;   // Set TC2 for Output Compare
  TIE |= 0x04;              // Enable interrupt for TC2
}

/// Function to count the number of times the interupt has been triggered, and calculate values for gyroscope using integration if it has.
interrupt 10 void TC2_ISR(void){
  TFLG1 = 0x04; // Set timer flag to 4
  TC2 = TCNT + GYRO_SAMPLE_PERIOD_CYCLES_TENTH; // Got a prescaler of 1 - can count when interrupt is triggered 10 times (0.01 seconds have elapsed)

  if(gyro_isr_count >= 10){ // if 0.01 seconds have elapsed
    l3g4200d_getrawdata(&gxraw,&gyraw,&gzraw) ; // Get the raw data                                                        //!< Read gyro data
    gyro_azimuth = gyro_azimuth - 0.00912*angularDisplacement(gxraw[0],GX_OFFSET,GYRO_SAMPLE_PERIOD);       // Convert to azimuth
    gyro_elevation = gyro_elevation + 0.00912*angularDisplacement(gyraw[0],GY_OFFSET,GYRO_SAMPLE_PERIOD);   // Convert to elevation
    gyro_isr_count = 0;     // Reset isr counter to zero
  }
  else{
    gyro_isr_count++;  // Otherwise increment isr counter by one
  }
}

/// Function to return the value for acceleration (uses the function to calculate acceleration)
float returnAccElevation(void){
  float acc_elevation;
  adxl345_getrawdata(&axraw,&ayraw,&azraw);         // Read accelerometer data
  acc_elevation = accElevation(axraw[0],azraw[0]);  // Convert to elevation
  return acc_elevation;                             // Return elevation
}

/// Function to return azimuth from the magnetometer
float returnMagAzimuth(void){
  float mag_azimuth;
  hm5883_getrawdata(&mxraw,&myraw,&mzraw);                    // Read magnetometer data
  mag_azimuth = magAzimuth(myraw[0],mzraw[0],mag_heading90);  // Convert to azimuth
  return mag_azimuth;                                         // Return azimuth
}

/// Function to return azimuth from the gyroscope
float returnGyroAzimuth(void){
  return gyro_azimuth;            // Return azimuth
}

/// Function to return the elevation from the gyroscope
float returnGyroElevation(void){
  return gyro_elevation;          // Return elevation
}

/// Function to move the PTU into the correct position to obtain the magnetometer's offset
void prepareMag(void){
  setAzimuth(90);      // Move the PTU to elevation = 0, azimuth = 90
  setElevation(0);
  delay1(1000); // Wait for the PTU to be in the correct spot

  hm5883_getrawdata(&mxraw, &myraw, &mzraw);  // Read magnetometer data
  mag_heading90 = magHeading(myraw[0],mzraw[0]);    // Record the data that corresponds this orientation
}

/// Function to move the PTU into the correct position to obtain the gyroscope's offset. Takes the azimuthStart variable as an argument.
void prepareGyro(int azimuthStart){
  setElevation(0);                            // Move PTU to starting position
  delay1(500);  // Delay to allow PTU to move
  setAzimuth(azimuthStart); //Set the azimuth to the azimuth start
  delay1(500);  // Delay

  gyro_azimuth = azimuthStart;    // Set starting orientation values to match starting positioin
  gyro_elevation = 0; // Set gyroscope elevation to 0, and gx and gy to zero
  gxraw[0] = 0;
  gyraw[0] = 0;

  initTC2();  // Start gyro interrupt and begin updating orientation
}

/**
 * Returns the value for the x coordinate of the laser's target.

 * Formula: x = range*cos(elevation*PI/180)*cos(azimuth*PI/180)
 * @param[in] range, elevation, azimuth
 * @param[out] x
*/
float returnX(float range,float elevation,float azimuth){
  float x;
  x = range*cos(elevation*PI/180)*cos(azimuth*PI/180);
  return x;
}

/**
 * Returns the value for the y coordinate of the laser's target.

 * Formula: y = range*cos(elevation*PI/180)*sin(azimuth*PI/180)
 * @param[in] range, elevation, azimuth
 * @param[out] y
*/
float returnY(float range,float elevation,float azimuth){
  float y;
  y = range*cos(elevation*PI/180)*sin(azimuth*PI/180);
  return y;
}

/**
 * Returns the value for the z coordinate of the laser's target.

 * Formula: z = range*sin(elevation*PI/180)
 * @param[in] range, elevation
 * @param[out] z
*/
float returnZ(float range,float elevation){
  float z;
  z = range*sin(elevation*PI/180);
  return z;
}
