#ifndef IMU_H
#define IMU_H

float accElevation(int,int);
float angularDisplacement(int,float,float);
float magAzimuth(int,int,float);
float magHeading(int,int);
void initTC2(void);
float returnAccElevation(void);
float returnMagAzimuth(void);
float returnGyroAzimuth(void);
float returnGyroElevation(void);
void prepareMag(void);
void prepareGyro(int);
float returnX(float,float,float);
float returnY(float,float,float);
float returnZ(float,float);

#define BUFF_SIZE	100 //!< Define maximum buffer size
#define PI 3.14159265 //!< Define value for PI
#define AX_OFFSET 3 //!< Define offset for the x axis of the accelerometer
#define AY_OFFSET 13 //!< Define offset for y axis of the accelerometer
#define AZ_OFFSET 11 //!< Define offset for z axis of the accelerometer
#define GYRO_SAMPLE_PERIOD 0.01                   //!< Update the gyro orientation at 100Hz
#define GYRO_SAMPLE_PERIOD_CYCLES_TENTH 24000 //!< Number of timer cycles for a tenth of the gyro sample period
#define GX_OFFSET 26 //!< Define offset for x axis of the gyroscope
#define GY_OFFSET -92 //!< Define offset for y axis of the gyroscope
#define GZ_OFFSET 5 //!< Define offset for z axis of the gyroscope

#endif