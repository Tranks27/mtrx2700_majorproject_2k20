#include <hidef.h>          /* common defines and macros */
#include "derivative.h"     /* derivative-specific definitions */
#include "display.h"
#include <math.h>

/*!
  * \file display.c
  * File that contains functions for operating LCD and 7seg displays
*/

extern float laserdata;

/// Lookup table for 7seg ASCII values
const int SEG_DISPLAY[16] = {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67,0x77,0x7C,0x39,0x5E,0x79,0x71};
const int SEG_ENABLE[4] = {0x0E,0x0D,0x0B,0x07};

/// 7seg count variable
static int count_7seg;
extern float map_laser_range;


// 7 Seg Display Functions //////////////////////////////////////////////////////////////////////////////////////


/// Initialise for 7seg LEDs
void init_7seg(void){
  DDRB = 0xFF;                // configure PORTB for output
  DDRJ = 0xFF;                // configure PTJ for output
  DDRP = 0xFF;                // configure PTP for output
  PTJ  = 0x00;                // enable LEDs
  count_7seg = 0;             // start at the 1st 7 seg display
}

/// Initialise timer4 for 7seg interrupt
void initTC4(void){
  TIOS |= 0x10;               // select channel 4 for output compare
  TSCR1 = 0x80;               // enable timers
  TSCR2 = 0x00;               // prescaler of 1
  TIE |= 0x10;                // enable timer interrupt for channel 4
}

/** 
* \brief Display data on the LED. 
* \details The given float value is divided up into four digits each of which 
* is shown at each interrupt call, but high enough frequency will make them appear simultaneous
*/
void print_7seg(float value){
  int first_digit;
  int first_remainder;
  int second_digit;
  int second_remainder;
  int third_digit;
  int fourth_digit;
  
  int segValue1;
  int segValue2;
  int segValue3;
  int segValue4; 
  int value_int;
  
  value_int = (int)(value*100);               // start converting the float into separate digits
  
  first_digit = value_int/1000;               // determine the digit to be displayed on the first 7seg
  first_remainder = value_int%1000;
        
  second_digit = first_remainder/100;         // determine the digit to be displayed on the second 7seg
  second_remainder = first_remainder%100;
          
  third_digit = second_remainder/10;                // determine the digit to be displayed on the third 7seg
          
  fourth_digit = second_remainder%10;               // determine the digit to be displayed on the fourth 7seg
          
  segValue1 = SEG_DISPLAY[first_digit];               // write to each 7 seg display
  segValue2 = SEG_DISPLAY[second_digit];
  segValue3 = SEG_DISPLAY[third_digit];
  segValue4 = SEG_DISPLAY[fourth_digit];
          
  switch(count_7seg){
    case 0:
          PTP = SEG_ENABLE[0];                // activate the first display
          PORTB = segValue1;                  // write to the first display
          count_7seg++;                       // next interrupt write to the next display
          
          break;
    case 1:
          PTP = SEG_ENABLE[1];                // activate the second display
          segValue2 |= DP;                    // turn on decimal point
          PORTB = segValue2;                  // write to the second display
          count_7seg++;                       // next interrupt write to the next display
          
          break;
    case 2:
          PTP = SEG_ENABLE[2];                // activate the third display
          PORTB = segValue3;                  // write to the third display
          count_7seg++;                       // next interrupt write to the next display
          
          break;
    case 3:
          PTP = SEG_ENABLE[3];                // activate the fourth display
          PORTB = segValue4;                  // write to the fourth display
          count_7seg = 0;                     // next interrupt write to the next display
          
          break;
  }
}

/// Interrupt callback for timer4
interrupt 12 void sevenSeg_isr(void){
  TC4 = TCNT + 24000;                     // interrupt at 0.001s or 100Hz
  TFLG1 = 0x10;                           // clear flag      
      print_7seg(map_laser_range);              // display laser range to 2dp  
}

// LCD Functions ///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the LCD
void init_LCD(void){

      DDRK = 0xFF;     // Port K output (check docs)
      COMWRT4(0x33);   //reset sequence provided by data sheet
      MSDelay(1);
      COMWRT4(0x32);   //reset sequence provided by data sheet
      MSDelay(1);
      COMWRT4(0x28);   //Function set to four bit data length
                                       //2 line, 5 x 7 dot format
      MSDelay(1);
      COMWRT4(0x06);  //entry mode set, increment, no shift
      MSDelay(1);
      COMWRT4(0x0E);  //Display set, disp on, cursor on, blink off
      MSDelay(1);
      COMWRT4(0x01);  //Clear display
      MSDelay(100);
}

/// clear lcd display
void LCD_clear(void){
    COMWRT4(0x01);   // Clear display
    MSDelay(100);
}

/// cursor off
void LCD_cursorOff(void){
    COMWRT4(0x0C);   // Display on, cursor off
    MSDelay(1);
}

/// Print a string to LCD
void printLCD(unsigned int row, unsigned int col, unsigned char *buffer){
    unsigned char* firstChar = buffer;
    int count = 0;

    // Set cursor position
    if (row == 1){
       COMWRT4(0x80 + col);  //set cursor to line 1, "col" position
       MSDelay(1);
    }else if(row == 2){
       COMWRT4(0xC0 + col);  //set cursor to line 2, "col" position
       MSDelay(1);     
    }
    // Start writing data until null
    while (*(firstChar + count) && count < 16){ 
       DATWRT4(*(firstChar + count));  // Write character to LCD
       count++;                        // increment buffer
       MSDelay(250);
    }
  
}

/// Write low-level commands to the LCD
void COMWRT4(unsigned char command){
    unsigned char x;
    
    x = (command & 0xF0) >> 2;         //shift high nibble to center of byte for Pk5-Pk2
    LCD_DATA =LCD_DATA & ~0x3C;          //clear bits Pk5-Pk2
    LCD_DATA = LCD_DATA | x;          //sends high nibble to PORTK
    MSDelay(1);
    LCD_CTRL = LCD_CTRL & ~RS;         //set RS to command (RS=0): command write
    MSDelay(1);
    LCD_CTRL = LCD_CTRL | EN;          //rais enable
    MSDelay(5);
    LCD_CTRL = LCD_CTRL & ~EN;         //Drop enable to capture command
    MSDelay(15);                       //wait
    x = (command & 0x0F)<< 2;          // shift low nibble to center of byte for Pk5-Pk2
    LCD_DATA =LCD_DATA & ~0x3C;         //clear bits Pk5-Pk2
    LCD_DATA =LCD_DATA | x;             //send low nibble to PORTK
    LCD_CTRL = LCD_CTRL | EN;          //rais enable // enable signal
    MSDelay(5);
    LCD_CTRL = LCD_CTRL & ~EN;         //drop enable to capture command
    MSDelay(15);
}

/// Write low-level Data to the LCD
void DATWRT4(unsigned char data){
    
    unsigned char x;

    x = (data & 0xF0) >> 2;
    LCD_DATA =LCD_DATA & ~0x3C;                     
    LCD_DATA = LCD_DATA | x;
    MSDelay(1);
    LCD_CTRL = LCD_CTRL | RS;           // set RS to data(RS=1): data write
    MSDelay(1);
    LCD_CTRL = LCD_CTRL | EN;           // enable signal
    MSDelay(1);
    LCD_CTRL = LCD_CTRL & ~EN;          // disable signal
    MSDelay(5);
   
    x = (data & 0x0F)<< 2;
    LCD_DATA =LCD_DATA & ~0x3C;                     
    LCD_DATA = LCD_DATA | x;
    LCD_CTRL = LCD_CTRL | EN;
    MSDelay(1);
    LCD_CTRL = LCD_CTRL & ~EN;
    MSDelay(15);
}

/// Convert float to string with 2dp
void float_to_str(float value,unsigned char* string){

  int value_int;
  char sign;
  int first_digit,first_remainder,second_digit,second_remainder;
  int third_digit,third_remainder,fourth_digit;
  
  value_int = (int)(fabs(value)*10);
  
  // determine sign of float
  if(value<0){
    sign = '-';
  } 
  else{
    sign = '+';
  }
  
  first_digit = value_int/1000;               // determine the digit to be displayed on the first 7seg
  first_remainder = value_int%1000;
  
  second_digit = first_remainder/100;         // determine the digit to be displayed on the second 7seg
  second_remainder = first_remainder%100;
  
  third_digit = second_remainder/10;          // determine the digit to be displayed on the third 7seg
  third_remainder = second_remainder%10;
  
  fourth_digit = third_remainder;             // determine the digit to be displayed on the fourth 7seg


  // convert integers to chars, and create the string that resembles a float number
  string[0] = sign;             // add the ascii value of '0' (i.e. 48) to make it char
  string[1] = first_digit + '0';
  string[2] = second_digit + '0';
  string[3] = third_digit + '0';
  string[4] = '.';
  string[5] = fourth_digit + '0';
  string[6] = '\0';
}

/// Millisecond delay
void MSDelay(unsigned int itime){
    unsigned int i,j;
    for(i=0;i<itime;i++)
      for(j=0;j<135;j++);
}