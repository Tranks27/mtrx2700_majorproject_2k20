#ifndef LASER_H
#define LASER_H

void initTC1(void);
unsigned long readPulseWidthCycles(void);
float laserRange(void);

#define TCNT_MAX 65536  //!< Maximum value of TCNT register
#define MAX_OVERFLOW 15 //!< Overflow corresponding to approximately 40m
#define MIN_RANGE_CYCLES 28235 //!< Timer cycles for reading of 1m
#define MAX_RANGE_CYCLES 142769 //!< Timer cycles for reading of 4m

#endif