#include <hidef.h>            /* common defines and macros */
#include "derivative.h"       /* derivative-specific definitions */
#include "laser.h"
#include <math.h>

/*!
  * \file laser.c
  * File that contains laser measurement and operation functions
*/

static unsigned int overflow;            //!< count of TCNT overflow during pulse-width measurement
static unsigned int edge1;               //!< TCNT value at rising edge of pulse-width
static unsigned int edge2;               //!< TCNT value at value edge of pulse-width
static unsigned long pulse_width;        //!< pulse-width value in timer cycles

/// Initialise TC1 for input capture, in order to measure the pulse-width of the signal from the laser
void initTC1(void){
  TSCR1 = 0x80;                   // timer enable
  TSCR2 = 0x00;                   // prescaler 1
  TIOS &= ~(TIOS_IOS1_MASK);      // set TC1 for Input Capture
  TCTL4 = 0x04;                   // initially only capture rising edge
  TIE |= 0x02;                    // enable interrupt for TC1  
}

/// Interrupt to measure the pulse width of the laser signal
interrupt 9 void TC1_ISR(void){
  unsigned int diff;
  unsigned long pulse_temp;
  TFLG1 = 0x02;                   // clear C1F flag

  if(TCTL4 == 0x04){              // if the interrupt was triggered by a rising edge    
    TCTL4 = 0x08;                 // TC1 interrupt now only triggers on a falling edge
    TFLG2 = 0x80;                 // clear the TOF flag
    TSCR2 |= 0x80;                // enable TCNT overflow interrupt
    edge1 = TC1;                  // save the first edge
    overflow = 0;                 // reset TCNT overflow counter
  } 
  
  else if(TCTL4 == 0x08){         // if the interrupt was triggered by a falling edge
    edge2 = TC1;
    TCTL4 = 0x04;                 // TC1 interrupt now only triggers on rising edge
    TSCR2 &= ~(0x80);             // disable TCNT overflow interrupt
                       
    diff = edge2-edge1;           // calculate and store the length of the pulse width
    if(edge2<edge1){
      overflow -= 1;
    }                             // a maximum number of timer overflows for the pulse width eliminates inaccurately large results
    if(overflow<MAX_OVERFLOW){    // update the pulse width if the pulse width is within device specified range of 40m
      pulse_temp = (unsigned long)overflow*TCNT_MAX + (unsigned long)diff;
      if(pulse_temp>MIN_RANGE_CYCLES && pulse_temp<MAX_RANGE_CYCLES){             // restrict pulse-width readings to between 1 and 4m
        pulse_width = pulse_temp;
      }
    }      
  }   
}


/// Interrupt to count how many times TCNT overflows during a pulse-width measurement
interrupt 16 void TOI_ISR(void){
  TFLG2 = 0x80;                   // clear the TOF flag
  overflow++;                     // increment TCNT overflow counter
}

/// Returns the currently stored pulse-width value in terms of timer cycles
unsigned long readPulseWidthCycles(void){
  unsigned long cycles;
  cycles = pulse_width;
  return cycles;                  // return the current pulse width
}

/// Converts a pulse-width in terms of timer cycles, to a measured range in metres
float laserRange(void){
  float range = fabs(0.0000429855*pulse_width-0.2137);
  return range;
}