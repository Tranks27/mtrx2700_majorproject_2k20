#include "config.h"
#include "menu_screen.h"

/*!
  * \file config.c
  * File that contains functions for configuring the mapping routine
*/

config mergedConfigs;       //!< copy of configuration settings created by merging the keypad and PC data
config primaryConfigs;      //!< configuration settings according to the PC
config secondaryConfigs;    //!< configuration settings according to the keypad

/// Updates the merged config with changes made to the primary and secondary configs, prioritising changes in the primary config
void updateConfigs(config* merged,config* primary,config* secondary){
  if((((*merged).headStart) != ((*primary).headStart))  ||          // if the primary and merged configs are different
  (((*merged).headEnd) != ((*primary).headEnd))         ||
  (((*merged).headStep) != ((*primary).headStep))       ||
  (((*merged).elevStart) != ((*primary).elevStart))     ||
  (((*merged).elevEnd) != ((*primary).elevEnd))         ||
  (((*merged).elevStep) != ((*primary).elevStep))       ||
  (((*merged).frequency) != ((*primary).frequency))     ||
  (((*merged).samples) != ((*primary).samples))         ||
  (((*merged).control) != ((*primary).control))         ||
  (((*merged).elevCalc) != ((*primary).elevCalc))       ||
  (((*merged).headCalc) != ((*primary).headCalc)) 
  ){
    *merged = *primary;                                             // set all configs to be equal to the primary config
    *secondary = *primary;
    refreshMenu();                                                  // refresh LCD menu
  }
  else if((((*merged).headStart) != ((*secondary).headStart)) ||    // otherwise, if the secondary and merged configs are different
  (((*merged).headEnd) != ((*secondary).headEnd))             ||
  (((*merged).headStep) != ((*secondary).headStep))           ||
  (((*merged).elevStart) != ((*secondary).elevStart))         ||
  (((*merged).elevEnd) != ((*secondary).elevEnd))             ||
  (((*merged).elevStep) != ((*secondary).elevStep))           ||
  (((*merged).frequency) != ((*secondary).frequency))         ||
  (((*merged).samples) != ((*secondary).samples))             ||
  (((*merged).control) != ((*secondary).control))             ||
  (((*merged).elevCalc) != ((*secondary).elevCalc))           ||
  (((*merged).headCalc) != ((*secondary).headCalc))
  ){
    *merged = *secondary;                                           // set all configs to be equal to the secondary config
    *primary = *secondary;
  }
}

// Sets initial values for a config data type
void initConfigVals(config *c){
  (*c).headStart = 0;
  (*c).headEnd = 0;
  (*c).headStep = 0;
  (*c).elevStart = 0;
  (*c).elevEnd = 0;
  (*c).elevStep = 0;
  (*c).frequency = 0;
  (*c).samples = 0;
  (*c).control = 0;
  (*c).elevCalc = 0;
  (*c).headCalc = 0;
}

