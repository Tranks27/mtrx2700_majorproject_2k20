#include <hidef.h>            /* common defines and macros */
#include "derivative.h"       /* derivative-specific definitions */
#include "servo.h"

/*!
  * \file servo.c
  * File that contains servo control functions
*/

/// Function to set the azimuth of the PTU. Takes an input in degrees
void setAzimuth(float degrees){
  unsigned int dutyCycle;

  //Ensure degree input lies in working domain and set it to the min or max if it doesn't
  if (degrees > DEGREES_MAX){
    degrees = DEGREES_MAX;
  }else if (degrees < DEGREES_MIN){
    degrees = DEGREES_MIN;
  }

  dutyCycle = (unsigned int)(1800 + 32*degrees);  // Calculate PWMDTY67 value
  PWMDTY6 = (char)((dutyCycle&0xFF00)>>8);        // Store higher byte
  PWMDTY7 = (char)(dutyCycle&0x00FF);             // store lower byte
}

/// Function to set the elevation of the PTU. Takes an input in degrees.
void setElevation(float degrees){
  unsigned int dutyCycle; // Variable for calculating duty cycle

  // Shift range from -90 to 90 -> 0 to 180
  degrees += 90;

  //Ensure degree input lies in working domain and set it to the min/max if it doesn't
  if (degrees > DEGREES_MAX){
    degrees = DEGREES_MAX;
  }else if (degrees < DEGREES_MIN){
    degrees = DEGREES_MIN;
  }

  dutyCycle = (unsigned int)(1500 + 32*degrees); // Calculate PWMDTY45 value
  PWMDTY4 = (char)((dutyCycle&0xFF00)>>8);// Store higher byte
  PWMDTY5 = (char)(dutyCycle&0x00FF);// Store lower byte
}


/// Function to initalise the PWM cycle
void initPWM(void) {
   PWMPOL   = 0b10100000;    // Invert polarity
   PWMCLK   = 0;            // Using clock A and B (not SA or SB)
   PWMPRCLK = 0x33;         // Setting prescaler for B and A to 8
   PWMCAE   = 0;            // Left aligned
   PWMCTL   = 0b11000000;   // Concatenates channels 6,7 and 4,5

   // Sets the period of concatenated channels 6 and 7 to 60000 (20ms)
   PWMPER6  = 0xEA;
   PWMPER7  = 0x60;
   PWMPER4  = 0xEA;
   PWMPER5  = 0x60;         // Sets the period of concatenated channels 4 and 5 to 60000(20ms)

   PWME     = 0b10100000;   // Enables PWM channels 7 and 5
}