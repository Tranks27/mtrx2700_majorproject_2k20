// header files
#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "serial.h"
#include "config.h"
#include "menu_screen.h"

/*!
  * \file serial.c
  * File that contains serial communication protocol for transmitting and receiving
*/

extern config mergedConfigs;     //!< copy of configuration settings created by merging the keypad and PC data
extern config primaryConfigs;    //!< configuration settings according to the PC
extern config secondaryConfigs;  //!< configuration settings according to the keypad
extern START;

// variables for SCI coms
int SCI_transmitting;            //!< 0 - board is not transmitting, 1- board is transmitting
int SCI_receiving;               //!< 0 - board is not receiving, 1- board is receiving
packet packetList[MAX_PACKETS];  //!< buffer of packets to be sent
int lenPacketList;               //!< length of the packet buffer
int currentPacket;               //!< index of current packet
int currentEnd;                  //!< index of the end of the packet list
int packetLen;                   //!< length of a packet in bytes
int packetI;                     //!< index for counting how many bytes of a packet are left to transmit
packet inputPacket;              //!< storage for an incoming packet
int inputPacketI;                //!< index for counting how many bytes of a packet are left to receive
char programState;               //!< state of the program - 'C' for configuration or 'S' for scanning
char messageIN;                  //!< storage for incoming character messages
char messageOUT;                 //!< storage for outgoing character messages

/// Initialises the SCI1 port
void init_SCI(void){
  // setting control register to empty
  // receiver/transmitter ON/OFF only affects their specific bits
  SCI1CR2 = 0x00;
  // set Baud register to 9600
  SCI1BDH = 0x00;
  SCI1BDL = 0x9C;
}

/// Initialises variables used during serial transmission
void init_serial(void){
  init_SCI();
  lenPacketList = 0;
  currentPacket = 0;
  currentEnd = 0;
  packetLen = sizeof(packet);
  packetI = packetLen;
  inputPacketI = packetLen;
  receiverOFF();
  transmitterOFF();
  programState = 'C';  // C - configuring, S - scanning
  messageIN = 'C';
  messageOUT = 'C';
  initConfigVals(&mergedConfigs);
  initConfigVals(&primaryConfigs);
  initConfigVals(&secondaryConfigs);
}

/// Turn on the receiver
void receiverON(void){
  SCI_receiving = 1;
  SCI1CR2 |= 0b00100100;
  // enable receiver and receiver interrupt
}

/// Turn off the receiver
void receiverOFF(void){
  SCI_receiving = 0;
  SCI1CR2 &= 0b11011011;               
  // disable receiver and receiver interrupt
}

/// Turn on the transmitter
void transmitterON(void){
  SCI_transmitting = 1;
  SCI1CR2 |= 0b10001000;               
  // enable transmitter and transmitter interrupt
}

/// Turn off the transmitter
void transmitterOFF(void){
  SCI_transmitting = 0;
  SCI1CR2 &= 0b01110111;               
  // disable transmitter and transmitter interrupt
}


/// Add a packet to the buffer to be sent
void addPacket(packet new_packet){
// adds packet <new_packet> to the list of packets to be sent
// maximum packets that can be in the waitlist is <MAX_PACKETS> (256)
// returns 1 if it fails 0 if it succeeds
  // makes sure that we dont overwrite packets that havent been sent
  if (lenPacketList >= MAX_PACKETS){ 
    // fails: waitlist full
    return;
    //return 1;
    // need to change back to int for error handling  
  }
  
  // add packet to the list
  packetList[currentEnd] = new_packet;
  
  // increments the size of the waitlist
  lenPacketList++;
  
  // put into transmission mode if not already
  if (lenPacketList == 1){
    receiverOFF();
    transmitterON();   
  }
    
  // sets the next available slot
  currentEnd++;
  if (currentEnd >= MAX_PACKETS){
    currentEnd = 0;  
  }
  // succedes: packet added to waitlist
  return;
  //return 0;
  // need to change back to int for error handling 
}

/// Create and add a config packet to the buffer
void addConfigPacket(config c){
  packet new_packet;
  new_packet.Pconfig.pack_type = 'C';
  if(START == true){
    new_packet.Pconfig.state = 'S';
  } 
  else{
    new_packet.Pconfig.state = 'C';
  }
  new_packet.Pconfig.configData = c;
  addPacket(new_packet); 
}

/// Create and add a float value packet to the buffer
void addValuePacket(char type, int groupID, float f0, float f1, float f2){
  packet new_packet;
  new_packet.Pfloat.pack_type = type; // P - position Coordinates, H - Heading, E - elevation and range
  new_packet.Pfloat.groupID = groupID; 
  new_packet.Pfloat.data[2] = f0;
  new_packet.Pfloat.data[1] = f1;
  new_packet.Pfloat.data[0] = f2;
  // data is sent in reverse order
  addPacket(new_packet);   
}

/// Transmit data in the SCI1 data register
void transmitData(unsigned char output){
  char status = SCI1SR1;        // read status register to clear TDRE flag
  SCI1DRL = output;             // send character out through SCI1
} 

/// Transmit current packet in the buffer
void transmitPacket(void){
  unsigned char data;
  if (lenPacketList <= 0){ // checks if there is anything to send
    transmitterOFF();
    if(programState=='C'){
      receiverON();
    }    
    return;
  }
  
  if (packetI == 0){ // checks if we have hit the last byte of the packet
    currentPacket++; // sets to next packet
    lenPacketList--; // decreases the amount of packets in the list
    packetI = packetLen; // returns to the first byte of the packet
    if (currentPacket >= MAX_PACKETS){
      currentPacket = 0;  
    }
    return;
  }else{
    // extracts the required byte from the data and transmits it
    data = packetList[currentPacket].chunks[packetI-1]; 
    transmitData(data);
    packetI--;
  }

}

/// Read data in the SCI1 data register
unsigned char readData(void){
  char status = SCI1SR1;
  unsigned char input = SCI1DRL;
  return input; 
}

/// Read the current incoming packet
void readPacket(void){
    inputPacket.chunks[inputPacketI-1] = readData();
    inputPacketI--;
    if(inputPacketI == 0){
      receiverOFF();
      actionInputPacket();
    }
}

/// Update program state and configuration according to input packet 
void actionInputPacket(void){ 
  primaryConfigs = inputPacket.Pconfig.configData;                // record config data from PC
  updateConfigs(&mergedConfigs,&primaryConfigs,&secondaryConfigs);   // merge with keypad config
  addConfigPacket(primaryConfigs);                                // send the updated config to the PC
  inputPacketI = packetLen;
  programState = inputPacket.Pconfig.state;                       // update program state
}

/// Interrupt for receiving and transmitting using SCI1
interrupt 21 void sci1_isr(void){
  if (programState == 'C'){  // config state
    if(SCI_receiving == 1){
      readPacket();          // read
    }
    if(SCI_transmitting == 1){
      transmitPacket();     // transmit
    }
  }else if (programState == 'S'){   // scanning state
    if(SCI_transmitting == 1){
      transmitPacket();    // transmit
    }
    if(SCI_receiving == 1){
       receiverOFF();     // flips back to transmitting
       transmitterON();
    }
  }
}