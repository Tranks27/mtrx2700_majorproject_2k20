#ifndef DISPLAY_H
#define DISPLAY_H

void init_7seg(void);
void print_7seg(float);
void initTC4(void);
#define DP 0x80     //!< Decimal point for 7seg

void init_LCD(void);
void printLCD(unsigned int, unsigned int, unsigned char*);
void LCD_clear(void);
void LCD_cursorOff(void);

void COMWRT4(unsigned char);
void DATWRT4(unsigned char);
void MSDelay(unsigned int);
void float_to_str(float,unsigned char*);

#define LCD_DATA PORTK    //!< PORTK is used for LCD data transfer
#define LCD_CTRL PORTK    //!< PORTK is also used for LCD commands
#define RS 0x01           //!< RS pin is 1st bit of PORTK
#define EN 0x02           //!< EN pin is 2st bit of PORTK

#endif