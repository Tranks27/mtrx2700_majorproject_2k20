# MTRX2700_Major_Project_2k20

How to clone git repo in ur local:

    git clone "repo_link_from_bitbucket.com"

    git status ~ to check commits

make changes to files and then

    git add .

    git commit -m "comments for editing"

To check commits and messages:

    git log

After changes are made, to update the repo for a branch:

    git push origin <branchName>


Now that you've synced your local repo, to check the status of the repo.

    git status

To change branches in your repo

    git checkout origin/branch_name

To make a permanent change in the master branch from a respective branch

    git merge origin/branch_name


ALSO, ALWAYS MAKE SURE YOU ARE IN THE REPOSITORY DRIVE IN YOUR TERMINAL WHEN PUSHING OR PULLING CHANGE.



Github setup for not entering password every time

    git config --global user.email "email@uni.sydney.edu.au"
    git config --global user.name "username"
